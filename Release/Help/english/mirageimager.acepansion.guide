@DATABASE mirageimager.acepansion.guide
@$VER: Mirage Imager 1.2 (21.10.2023)
@INDEX Main
@TOC Main
@AUTHOR Philippe Rimauro
@MASTER ACE.guide

@NODE Main
@TITLE "ACE � Mirage Imager expansion plugin"


    @{b}Mirage Imager expansion plugin 1.2@{ub}
    Plugin to emulate the Mirage Imager from Mirage Microcomputers Ltd.

    Copyright � 2021-2023 Philippe Rimauro
    All rights reserved.


    @{u}Description@{uu}

    This plugin emulates the Mirage Imager from Mirage Microcomputers Ltd.

    The Mirage Imager contains an 8KiB ROM for which several versions are
    available. The most common is provided with this plugin, but if you want
    to use another one, just replace the file "mirageimager.rom" from the
    "plugins" drawer with a new one.


    @{u}Tooltypes@{uu}

    None.


   @{u}Credits@{uu}

     @{b}Code:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
     @{b}Icon:@{ub}
        @{b}*@{ub} Christophe @{i}'Highlander'@{ui} Delorme - @{"mailto:chris.highlander@free.fr" system "openurl mailto:chris.highlander@free.fr"}
     @{b}Documentation:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
     @{b}Translations:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
        @{b}*@{ub} Stefan @{i}'polluks'@{ui} Haubenthal - @{"mailto:polluks@sdf.lonestar.org" system "openurl mailto:polluks@sdf.lonestar.org"}
        @{b}*@{ub} Juan Carlos Herran Martin - @{"mailto:juancarlos@morguesoft.eu" system "openurl mailto:juancarlos@morguesoft.eu"}

@ENDNODE

