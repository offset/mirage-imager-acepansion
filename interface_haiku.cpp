/*
** Graphical User Interface using MUI
** for Mirage Imager ACEpansion
*/

#include <Alert.h>
#include <Box.h>
#include <Button.h>
#include <GroupView.h>
#include <LayoutBuilder.h>
#include <String.h>

#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */

class ColorButton: public BButton
{
	public:
	ColorButton(const char* label, uint32 msg)
		: BButton("button", label, new BMessage(msg))
	{
	}

	void AttachedToWindow() override
	{
		BButton::AttachedToWindow();

		SetLowColor(make_color(255,64,64));
	}

};

class ButtonGroup : public BBox {
public:
    ButtonGroup(const char* name, struct ACEpansionPlugin* plugin)
        : BBox("button group")
        , myPlugin(plugin)
    {
        SetLabel(name);
        BGroupView* inner = new BGroupView(B_VERTICAL);
        AddChild(inner);

        BLayoutBuilder::Group<>(inner)
            .SetInsets(B_USE_DEFAULT_SPACING)
            .Add(BTN_Stop = new ColorButton("  STOP  ", 'STOP'))
        .End();
    }

    void AttachedToWindow()
    {
        BTN_Stop->SetLowColor(make_color(255,64,64));
        BTN_Stop->SetTarget(this);
    }

    void MessageReceived(BMessage* message)
    {
        switch(message->what) {
            case 'STOP':
                Plugin_TriggerStop(myPlugin);
                break;
            default:
                BBox::MessageReceived(message);
        }
    }

private:
    struct ACEpansionPlugin* myPlugin;
    BButton* BTN_Stop;
};

/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
	return TRUE;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
	return (Object*)new ButtonGroup(GetString(MSG_CONTROLS), plugin);
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
	BView* view = (BView*)gui;
	BWindow* window = view->Window();
	window->Lock();
	view->RemoveSelf();
	window->Unlock();
	delete view;
}



/*
** Functions called from the plugin
*/
VOID GUI_NotifyROMNotFound(UNUSED Object *gui, CONST_STRPTR rom)
{
	BString str;
	str.SetToFormat(GetString(MSG_NO_ROM), rom);
	BAlert* alert = new BAlert(GetString(MSG_TITLE), str.String(), GetString(MSG_OK));
	alert->Go();
}

