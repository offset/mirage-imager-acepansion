/*
** mirageimager.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


#define LIBNAME "mirageimager.acepansion"
#define VERSION 1
#define REVISION 2
#define DATE "21.10.2023"
#define COPYRIGHT "� 2021-2023 Philippe Rimauro"

#define API_VERSION 7


/*
** Mirage Imager plugin specific API to be called from GUI
*/
VOID Plugin_TriggerStop(struct ACEpansionPlugin *plugin);


#endif /* ACEPANSION_H */

