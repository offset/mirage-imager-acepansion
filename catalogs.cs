## Version $VER: mirageimager.acepansion.catalog 1.0 (19.08.2021)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a Mirage Imager
Brancher un Mirage Imager
Mirage Imager anschlie�en
Conecte un Mirage Imager
;
MSG_TITLE
Mirage Imager
Mirage Imager
Mirage Imager
Mirage Imager
;
MSG_MENU_PREFS
Mirage Imager controls...
Contr�les du Mirage Imager...
Mirage Imager Steuerung ...
Controles del Mirage Imager...
;
MSG_CONTROLS
Controls
Contr�les
Steuerung
Controles
;
MSG_NO_ROM
\ec\e8\ebMirage Imager ROM file is missing:\e2\en\n%s
\ec\e8\ebLe fichier de la ROM Mirage Imager est manquant :\e2\en\n%s
\ec\e8\ebMirage Imager ROM-Datei fehlt:\e2\en\n%s
\ec\e8\ebEl fichero de la ROM Multiface Two se ha perdido :\e2\en\n%s
;
MSG_OK
  Ok\x20\x20
  Ok\x20\x20
  Ok\x20\x20
  Ok\x20\x20

