/*
** Graphical User Interface using MUI
** for Mirage Imager ACEpansion
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>

#include <libraries/asl.h>
#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *IntuitionBase;
struct Library *UtilityBase;
struct Library *MUIMasterBase;



struct MUI_CustomClass *MCC_MirageImagerClass = NULL;



/*
** MCC_MirageImagerClass definitions
*/
#define MirageImagerObject NewObject(MCC_MirageImagerClass->mcc_Class, NULL

#define MUIA_MirageImager_Plugin (TAG_USER | 0x0000) // [I..] struct ACEpansionPlugin *, Reference to the plugin managing the GUI (mandatory)

#define MUIM_MirageImager_Notify (TAG_USER | 0x0100) // struct MUIP_MirageImager_Notify, notify the plugin or the GUI about changes
    #define MUIV_MirageImager_Notify_ToGUI    0 // Notify the GUI
    #define MUIV_MirageImager_Notify_ToPlugin 1 // Notify the plugin
        // For both directions
        #define MUIV_MirageImager_Notify_Reset 10 // Notify about RESET button pressed (no argument)
        #define MUIV_MirageImager_Notify_Stop  11 // Notify about STOP button pressed (no argument)

struct MUIP_MirageImager_Notify
{
    IPTR id;
    IPTR direction;
    IPTR type;
    CONST_STRPTR path;
};

struct MirageImagerData
{
    struct ACEpansionPlugin *plugin;
};



/*
** Helper functions
*/
static ULONG red[3] = { 0xffffffff,0x00000000,0x00000000 };

static Object * makeButton(ULONG c[3], CONST_STRPTR t)
{
    return VGroup,
        ButtonFrame,
        MUIA_Font, MUIV_Font_Button,
        MUIA_Font         , MUIV_Font_Tiny,
        MUIA_InputMode    , MUIV_InputMode_RelVerify,
        MUIA_Background   , MUII_ButtonBack,
        MUIA_CycleChain   , TRUE,
        Child, HVSpace,
        Child, HGroup,
            Child, HVSpace,
            Child, ColorfieldObject,
                MUIA_FixWidthTxt, "OO",
                MUIA_FixHeightTxt, " ",
                MUIA_Colorfield_RGB, c,
                End,
            Child, HVSpace,
            End,
        Child, TextObject,
            MUIA_Text_PreParse, MUIX_C,
            MUIA_Text_Contents, t,
            End,
        Child, HVSpace,
        End;
}



/*
** MCC_MirageImagerClass methods
*/
static IPTR mNotify(struct IClass *cl, Object *obj, struct MUIP_MirageImager_Notify *msg)
{
    struct MirageImagerData *data = INST_DATA(cl,obj);

    switch(msg->direction)
    {
        case MUIV_MirageImager_Notify_ToGUI:
            break;

        case MUIV_MirageImager_Notify_ToPlugin:
            switch(msg->type)
            {
                case MUIV_MirageImager_Notify_Stop:
                    Plugin_TriggerStop(data->plugin);
                    break;
            }
            break;
    }

    return 0;
}

static IPTR mNew(struct IClass *cl, Object *obj, Msg msg)
{
    struct TagItem *tags,*tag;
    struct MirageImagerData *data;

    struct ACEpansionPlugin *plugin = NULL;

    Object *BTN_Stop;

    // Parse initial taglist
    for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
    {
        switch(tag->ti_Tag)
        {
            case MUIA_MirageImager_Plugin:
                plugin = (struct ACEpansionPlugin *)tag->ti_Data;
                break;                              
        }
    }

    if(!plugin) return 0;

    obj = (Object *)DoSuperNew(cl,obj,
        GroupFrameT(GetString(MSG_CONTROLS)),
        Child, BTN_Stop  = makeButton(red, "  STOP  "),
        TAG_MORE,((struct opSet *)msg)->ops_AttrList);

    if(!obj) return 0;

    data = INST_DATA(cl,obj);

    data->plugin = plugin;

    // Buttons from GUI to plugin
    DoMethod(BTN_Stop, MUIM_Notify, MUIA_Pressed, FALSE,
             obj, 3, MUIM_MirageImager_Notify, MUIV_MirageImager_Notify_ToPlugin,
                     MUIV_MirageImager_Notify_Stop);

    return (IPTR)obj;
}



/*
** MUI Custom Class dispatcher, creation & destruction
*/
DISPATCHER(MirageImagerClass)
{
    switch (msg->MethodID)
    {
        case OM_NEW                   : return(mNew   (cl,obj,(APTR)msg));
        case MUIM_MirageImager_Notify : return(mNotify(cl,obj,(APTR)msg));
    }
}
DISPATCHER_END



/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
    UtilityBase = OpenLibrary("utility.library", 0L);
    IntuitionBase = OpenLibrary("intuition.library", 0L);
    MUIMasterBase = OpenLibrary("muimaster.library", 0L);

    if(UtilityBase && IntuitionBase && MUIMasterBase)
    {
        MCC_MirageImagerClass = MUI_CreateCustomClass(
            NULL,
            MUIC_Group,
            NULL,
            sizeof(struct MirageImagerData),
            DISPATCHER_REF(MirageImagerClass));
    }

    return MCC_MirageImagerClass != NULL;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
    if(MCC_MirageImagerClass)
        MUI_DeleteCustomClass(MCC_MirageImagerClass);

    CloseLibrary(MUIMasterBase);
    CloseLibrary(IntuitionBase);
    CloseLibrary(UtilityBase);
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
    return MirageImagerObject,
        MUIA_MirageImager_Plugin, plugin,
        End;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
    MUI_DisposeObject(gui);
}



/*
** Functions called from the plugin
*/
VOID GUI_NotifyROMNotFound(UNUSED Object *gui, CONST_STRPTR rom)
{
    MUI_Request(NULL, NULL, 0, GetString(MSG_TITLE), GetString(MSG_OK), GetString(MSG_NO_ROM), rom);
}

