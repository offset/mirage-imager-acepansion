dZ80 2.0, Copyright 1996-2002 by Mark Incley.

       Input file: mirageimager.rom

Disassembly range: 0000h - 1fffh

Pass 1 of 1
0000 1841      jr      0043h
0002 ff        rst     38h
0003 ff        rst     38h
0004 0100fe    ld      bc,0fe00h
0007 ed49      out     (c),c
0009 0105fc    ld      bc,0fc05h
000c ed78      in      a,(c)
000e e603      and     03h
0010 f684      or      84h
0012 329ba3    ld      (0a39bh),a
0015 c9        ret     
0016 c9        ret     
0017 00        nop     
0018 c3be1c    jp      1cbeh
001b 00        nop     
001c 00        nop     
001d 00        nop     
001e e9        jp      (hl)
001f 00        nop     
0020 7e        ld      a,(hl)
0021 c9        ret     
0022 dde9      jp      (ix)
0024 00        nop     
0025 00        nop     
0026 00        nop     
0027 00        nop     
0028 cd8a1a    call    1a8ah
002b c3b91a    jp      1ab9h
002e 00        nop     
002f 00        nop     
0030 c3b91a    jp      1ab9h
0033 2e49      ld      l,49h
0035 4d        ld      c,l
0036 47        ld      b,a
0037 00        nop     
0038 c3961b    jp      1b96h
003b 4d        ld      c,l
003c 42        ld      b,d
003d 44        ld      b,h
003e 314d42    ld      sp,424dh
0041 44        ld      b,h
0042 33        inc     sp
0043 f5        push    af
0044 3e02      ld      a,02h
0046 32ffbf    ld      (0bfffh),a
0049 f1        pop     af
004a fb        ei      
004b c9        ret     
004c 3a00a3    ld      a,(0a300h)
004f a7        and     a
0050 c9        ret     
0051 3a6da3    ld      a,(0a36dh)
0054 a7        and     a
0055 c9        ret     
0056 3a6ea3    ld      a,(0a36eh)
0059 a7        and     a
005a c9        ret     
005b 3a67a3    ld      a,(0a367h)
005e a7        and     a
005f c9        ret     
0060 3a8ea3    ld      a,(0a38eh)
0063 a7        and     a
0064 c9        ret     
0065 00        nop     

0066 f5        push    af
0067 c5        push    bc
0068 0100fc    ld      bc,&fc00
006b ed5f      ld      a,r
006d ed79      out     (c),a
006f 0c        inc     c
0070 ed59      out     (c),e
0072 0c        inc     c
0073 ed51      out     (c),d
0075 0c        inc     c
0076 ed69      out     (c),l
0078 0c        inc     c
0079 ed61      out     (c),h

007b 21e8bf    ld      hl,&bfe8
007e 0100fd    ld      bc,&fd00

0081 7e        ld      a,(hl)
0082 ed79      out     (c),a
0084 23        inc     hl
0085 03        inc     bc
0086 7d        ld      a,l
0087 a7        and     a
0088 20f7      jr      nz,&0081

008a ed73fcbf  ld      (&bffc),sp
008e 31fcbf    ld      sp,&bffc
0091 ed57      ld      a,i
0093 f5        push    af
0094 e1        pop     hl
0095 0108fc    ld      bc,&fc08
0098 ed69      out     (c),l
009a 03        inc     bc
009b ed61      out     (c),h
009d af        xor     a
009e 32febf    ld      (&bffe),a
00a1 3e01      ld      a,&01
00a3 32ffbf    ld      (&bfff),a
00a6 3e01      ld      a,&01
00a8 ed47      ld      i,a
00aa 0105bc    ld      bc,&bc05
00ad ed49      out     (c),c
00af 0100bd    ld      bc,&bd00
00b2 ed49      out     (c),c
00b4 0182f7    ld      bc,&f782
00b7 ed49      out     (c),c

00b9 01fff5    ld      bc,&f5ff
00bc 1600      ld      d,&00
00be fb        ei      
00bf 76        halt    

00c0 76        halt    
00c1 14        inc     d
00c2 2805      jr      z,&0c9
00c4 ed78      in      a,(c)
00c6 1f        rra     
00c7 30f7      jr      nc,&00c0

00c9 f3        di      
00ca ed56      im      1
00cc 010afc    ld      bc,&fc0a
00cf ed51      out     (c),d
00d1 0107fc    ld      bc,&fc07
00d4 3affbf    ld      a,(&bfff)
00d7 ed79      out     (c),a
00d9 ed7bfcbf  ld      sp,(&bffc)

00dd 21e8bf    ld      hl,&bfe8
00e0 0100fd    ld      bc,&fd00

00e3 ed78      in      a,(c)
00e5 77        ld      (hl),a
00e6 23        inc     hl
00e7 03        inc     bc
00e8 7d        ld      a,l
00e9 a7        and     a
00ea 20f7      jr      nz,&00e3

00ec c30102    jp      &0201

00ef 3a69a3    ld      a,(0a369h)
00f2 a7        and     a
00f3 c8        ret     z
00f4 ef        rst     28h
00f5 95        sub     l
00f6 1f        rra     
00f7 f7        rst     30h
00f8 0e1f      ld      c,1fh
00fa cd1114    call    1411h
00fd 37        scf     
00fe c9        ret     
00ff 00        nop     
0100 00        nop     
0101 00        nop     
0102 00        nop     
0103 00        nop     
0104 00        nop     
0105 00        nop     
0106 00        nop     
0107 00        nop     
0108 00        nop     
0109 00        nop     
010a 00        nop     
010b 00        nop     
010c 00        nop     
010d 00        nop     
010e 00        nop     
010f 00        nop     
0110 00        nop     
0111 00        nop     
0112 00        nop     
0113 00        nop     
0114 00        nop     
0115 00        nop     
0116 00        nop     
0117 00        nop     
0118 00        nop     
0119 00        nop     
011a 00        nop     
011b 00        nop     
011c 00        nop     
011d 00        nop     
011e 00        nop     
011f 00        nop     
0120 00        nop     
0121 00        nop     
0122 00        nop     
0123 00        nop     
0124 00        nop     
0125 00        nop     
0126 00        nop     
0127 00        nop     
0128 00        nop     
0129 00        nop     
012a 00        nop     
012b 00        nop     
012c 00        nop     
012d 00        nop     
012e 00        nop     
012f 00        nop     
0130 00        nop     
0131 00        nop     
0132 00        nop     
0133 00        nop     
0134 00        nop     
0135 00        nop     
0136 00        nop     
0137 00        nop     
0138 00        nop     
0139 00        nop     
013a 00        nop     
013b 00        nop     
013c 00        nop     
013d 00        nop     
013e 00        nop     
013f 00        nop     
0140 00        nop     
0141 00        nop     
0142 00        nop     
0143 00        nop     
0144 00        nop     
0145 00        nop     
0146 00        nop     
0147 00        nop     
0148 00        nop     
0149 00        nop     
014a 00        nop     
014b 00        nop     
014c 00        nop     
014d 00        nop     
014e 00        nop     
014f 00        nop     
0150 00        nop     
0151 00        nop     
0152 00        nop     
0153 00        nop     
0154 00        nop     
0155 00        nop     
0156 00        nop     
0157 00        nop     
0158 00        nop     
0159 00        nop     
015a 00        nop     
015b 00        nop     
015c 00        nop     
015d 00        nop     
015e 00        nop     
015f 00        nop     
0160 00        nop     
0161 00        nop     
0162 00        nop     
0163 00        nop     
0164 00        nop     
0165 00        nop     
0166 00        nop     
0167 00        nop     
0168 00        nop     
0169 00        nop     
016a 00        nop     
016b 00        nop     
016c 00        nop     
016d 00        nop     
016e 00        nop     
016f 00        nop     
0170 00        nop     
0171 00        nop     
0172 00        nop     
0173 00        nop     
0174 00        nop     
0175 00        nop     
0176 00        nop     
0177 00        nop     
0178 00        nop     
0179 00        nop     
017a 00        nop     
017b 00        nop     
017c 00        nop     
017d 00        nop     
017e 00        nop     
017f 00        nop     
0180 00        nop     
0181 00        nop     
0182 00        nop     
0183 00        nop     
0184 00        nop     
0185 00        nop     
0186 00        nop     
0187 00        nop     
0188 00        nop     
0189 00        nop     
018a 00        nop     
018b 00        nop     
018c 00        nop     
018d 00        nop     
018e 00        nop     
018f 00        nop     
0190 00        nop     
0191 00        nop     
0192 00        nop     
0193 00        nop     
0194 00        nop     
0195 00        nop     
0196 00        nop     
0197 00        nop     
0198 00        nop     
0199 00        nop     
019a 00        nop     
019b 00        nop     
019c 00        nop     
019d 00        nop     
019e 00        nop     
019f 00        nop     
01a0 00        nop     
01a1 00        nop     
01a2 00        nop     
01a3 00        nop     
01a4 00        nop     
01a5 00        nop     
01a6 00        nop     
01a7 00        nop     
01a8 00        nop     
01a9 00        nop     
01aa 00        nop     
01ab 00        nop     
01ac 00        nop     
01ad 00        nop     
01ae 00        nop     
01af 00        nop     
01b0 00        nop     
01b1 00        nop     
01b2 00        nop     
01b3 00        nop     
01b4 00        nop     
01b5 00        nop     
01b6 00        nop     
01b7 00        nop     
01b8 00        nop     
01b9 00        nop     
01ba 00        nop     
01bb 00        nop     
01bc 00        nop     
01bd 00        nop     
01be 00        nop     
01bf 00        nop     
01c0 00        nop     
01c1 00        nop     
01c2 00        nop     
01c3 00        nop     
01c4 00        nop     
01c5 00        nop     
01c6 00        nop     
01c7 00        nop     
01c8 00        nop     
01c9 00        nop     
01ca 00        nop     
01cb 00        nop     
01cc 00        nop     
01cd 00        nop     
01ce 00        nop     
01cf 00        nop     
01d0 00        nop     
01d1 00        nop     
01d2 00        nop     
01d3 00        nop     
01d4 00        nop     
01d5 00        nop     
01d6 00        nop     
01d7 00        nop     
01d8 00        nop     
01d9 00        nop     
01da 00        nop     
01db 00        nop     
01dc 00        nop     
01dd 00        nop     
01de 00        nop     
01df 00        nop     
01e0 00        nop     
01e1 00        nop     
01e2 00        nop     
01e3 00        nop     
01e4 00        nop     
01e5 00        nop     
01e6 00        nop     
01e7 00        nop     
01e8 00        nop     
01e9 00        nop     
01ea 00        nop     
01eb 00        nop     
01ec 00        nop     
01ed 00        nop     
01ee 00        nop     
01ef 00        nop     
01f0 00        nop     
01f1 00        nop     
01f2 00        nop     
01f3 00        nop     
01f4 00        nop     
01f5 00        nop     
01f6 00        nop     
01f7 00        nop     
01f8 00        nop     
01f9 00        nop     
01fa 00        nop     
01fb 00        nop     
01fc 00        nop     
01fd 00        nop     
01fe 00        nop     
01ff 00        nop     
0200 00        nop     

0201 0100fc    ld      bc,&fc00
0204 ed78      in      a,(c)
0206 57        ld      d,a
0207 e680      and     &80
0209 5f        ld      e,a
020a 7a        ld      a,d
020b f680      or      &80
020d d60f      sub     &0f
020f e67f      and     &7f
0211 b3        or      e
0212 ed79      out     (c),a

0214 0100fd    ld      bc,&fd00

0217 ed49      out     (c),c
0219 0c        inc     c
021a 20fb      jr      nz,&0217

021c 1600      ld      d,&00

021e 0100fe    ld      bc,&fe00
0221 ed51      out     (c),d
0223 0100fd    ld      bc,&fd00
0226 ed78      in      a,(c)
0228 b9        cp      c
0229 2803      jr      z,&022e
022b 14        inc     d
022c 18f0      jr      &021e
022e 0c        inc     c
022f 20f5      jr      nz,&0226

0231 210b00    ld      hl,&000b

0234 06fe      ld      b,&fe
0236 4a        ld      c,d
0237 ed49      out     (c),c
0239 06fc      ld      b,&fc
023b 4c        ld      c,h
023c ed78      in      a,(c)
023e 0100fe    ld      bc,&fe00
0241 ed49      out     (c),c
0243 06fc      ld      b,&fc
0245 4c        ld      c,h
0246 ed79      out     (c),a
0248 24        inc     h
0249 2d        dec     l
024a 20e8      jr      nz,&234

024c 0e05      ld      c,&05
024e ed51      out     (c),d
0250 067f      ld      b,&7f
0252 7a        ld      a,d
0253 e603      and     &03
0255 f68c      or      &8c
0257 ed79      out     (c),a

0259 010cfc    ld      bc,&fc0c
025c 213b00    ld      hl,&003b
025f 110040    ld      de,&4000

0262 1a        ld      a,(de)
0263 ed79      out     (c),a
0265 7e        ld      a,(hl)
0266 12        ld      (de),a
0267 23        inc     hl
0268 13        inc     de
0269 0c        inc     c
026a 79        ld      a,c
026b fe10      cp      &10
026d 20f3      jr      nz,&0262

026f 1100c0    ld      de,&c000
0272 1a        ld      a,(de)
0273 ed79      out     (c),a
0275 7e        ld      a,(hl)
0276 12        ld      (de),a
0277 23        inc     hl
0278 13        inc     de
0279 0c        inc     c
027a 79        ld      a,c
027b fe14      cp      14h
027d 20f3      jr      nz,0272h
027f 01c07f    ld      bc,7fc0h
0282 ed49      out     (c),c
0284 2100bf    ld      hl,0bf00h
0287 0100fd    ld      bc,0fd00h
028a 7e        ld      a,(hl)
028b ed79      out     (c),a
028d 23        inc     hl
028e 0c        inc     c
028f c28a02    jp      nz,028ah
0292 ed73fcbf  ld      (0bffch),sp
0296 31fcbf    ld      sp,0bffch
0299 0108fc    ld      bc,0fc08h
029c ed68      in      l,(c)
029e 03        inc     bc
029f ed60      in      h,(c)
02a1 e5        push    hl
02a2 08        ex      af,af'
02a3 d9        exx     
02a4 f5        push    af
02a5 c5        push    bc
02a6 d5        push    de
02a7 e5        push    hl
02a8 dde5      push    ix
02aa fde5      push    iy
02ac cdf109    call    09f1h
02af 2100a1    ld      hl,0a100h
02b2 cdcf19    call    19cfh
02b5 cd840a    call    0a84h
02b8 060f      ld      b,0fh
02ba dd21dc02  ld      ix,02dch
02be dd6e00    ld      l,(ix+00h)
02c1 dd6601    ld      h,(ix+01h)
02c4 36c3      ld      (hl),0c3h
02c6 23        inc     hl
02c7 dd7e02    ld      a,(ix+02h)
02ca 77        ld      (hl),a
02cb 23        inc     hl
02cc dd7e03    ld      a,(ix+03h)
02cf 77        ld      (hl),a
02d0 dd23      inc     ix
02d2 dd23      inc     ix
02d4 dd23      inc     ix
02d6 dd23      inc     ix
02d8 10e4      djnz    02beh
02da 183c      jr      0318h
02dc 12        ld      (de),a
02dd b9        cp      c
02de 60        ld      h,b
02df 1c        inc     e
02e0 ef        rst     28h
02e1 bc        cp      h
02e2 63        ld      h,e
02e3 1c        inc     e
02e4 b4        or      h
02e5 bb        cp      e
02e6 e61c      and     1ch
02e8 69        ld      l,c
02e9 bb        cp      e
02ea 9b        sbc     a,e
02eb 1c        inc     e
02ec 87        add     a,a
02ed bb        cp      e
02ee aa        xor     d
02ef 1c        inc     e
02f0 6f        ld      l,a
02f1 bb        cp      e
02f2 a4        and     h
02f3 1c        inc     e
02f4 72        ld      (hl),d
02f5 bb        cp      e
02f6 a9        xor     c
02f7 1c        inc     e
02f8 75        ld      (hl),l
02f9 bb        cp      e
02fa a9        xor     c
02fb 1c        inc     e
02fc 78        ld      a,b
02fd bb        cp      e
02fe ac        xor     h
02ff 1c        inc     e
0300 5a        ld      e,d
0301 bb        cp      e
0302 b1        or      c
0303 1c        inc     e
0304 5d        ld      e,l
0305 bb        cp      e
0306 b1        or      c
0307 1c        inc     e
0308 ecbc75    call    pe,75bch
030b 1c        inc     e
030c e9        jp      (hl)
030d bc        cp      h
030e 7a        ld      a,d
030f 1c        inc     e
0310 1b        dec     de
0311 b9        cp      c
0312 95        sub     l
0313 1c        inc     e
0314 1eb9      ld      e,0b9h
0316 98        sbc     a,b
0317 1c        inc     e
0318 0100f4    ld      bc,0f400h
031b ed49      out     (c),c
031d 0100f6    ld      bc,0f600h
0320 ed49      out     (c),c
0322 110800    ld      de,0008h
0325 cd3703    call    0337h
0328 0e07      ld      c,07h
032a 3e3f      ld      a,3fh
032c cd4613    call    1346h
032f 11030a    ld      de,0a03h
0332 cd3703    call    0337h
0335 180a      jr      0341h
0337 af        xor     a
0338 4a        ld      c,d
0339 cd4613    call    1346h
033c 14        inc     d
033d 1d        dec     e
033e 20f8      jr      nz,0338h
0340 c9        ret     
0341 01fff8    ld      bc,0f8ffh
0344 ed49      out     (c),c
0346 3e28      ld      a,28h
0348 3227a2    ld      (0a227h),a
034b 3e19      ld      a,19h
034d 3228a2    ld      (0a228h),a
0350 3e12      ld      a,12h
0352 323ea2    ld      (0a23eh),a
0355 2100c0    ld      hl,0c000h
0358 223aa2    ld      (0a23ah),hl
035b 06f5      ld      b,0f5h
035d ed78      in      a,(c)
035f e610      and     10h
0361 217a03    ld      hl,037ah
0364 2003      jr      nz,0369h
0366 218a03    ld      hl,038ah
0369 010fbc    ld      bc,0bc0fh
036c ed49      out     (c),c
036e 7e        ld      a,(hl)
036f 23        inc     hl
0370 04        inc     b
0371 ed79      out     (c),a
0373 05        dec     b
0374 0d        dec     c
0375 f26c03    jp      p,036ch
0378 1820      jr      039ah
037a 00        nop     
037b c0        ret     nz
037c 00        nop     
037d 3000      jr      nc,037fh
037f 00        nop     
0380 07        rlca    
0381 00        nop     
0382 1e19      ld      e,19h
0384 00        nop     
0385 268e      ld      h,8eh
0387 2e28      ld      l,28h
0389 3f        ccf     
038a 00        nop     
038b c0        ret     nz
038c 00        nop     
038d 3000      jr      nc,038fh
038f 00        nop     
0390 07        rlca    
0391 00        nop     
0392 1b        dec     de
0393 19        add     hl,de
0394 061f      ld      b,1fh
0396 8e        adc     a,(hl)
0397 2e28      ld      l,28h
0399 3f        ccf     
039a 21b103    ld      hl,03b1h
039d 1129a2    ld      de,0a229h
03a0 011100    ld      bc,0011h
03a3 edb0      ldir    
03a5 cd0400    call    0004h
03a8 067f      ld      b,7fh
03aa ed79      out     (c),a
03ac cdd813    call    13d8h
03af 1811      jr      03c2h
03b1 44        ld      b,h
03b2 57        ld      d,a
03b3 44        ld      b,h
03b4 59        ld      e,c
03b5 52        ld      d,d
03b6 47        ld      b,a
03b7 5f        ld      e,a
03b8 5e        ld      e,(hl)
03b9 46        ld      b,(hl)
03ba 4d        ld      c,l
03bb 55        ld      d,l
03bc 54        ld      d,h
03bd 4b        ld      c,e
03be 4c        ld      c,h
03bf 53        ld      d,e
03c0 4a        ld      c,d
03c1 44        ld      b,h
03c2 af        xor     a
03c3 3200a3    ld      (0a300h),a
03c6 32febf    ld      (0bffeh),a
03c9 0107df    ld      bc,0df07h
03cc ed49      out     (c),c
03ce 2a04c0    ld      hl,(0c004h)
03d1 110804    ld      de,0408h
03d4 0607      ld      b,07h
03d6 1a        ld      a,(de)
03d7 be        cp      (hl)
03d8 2807      jr      z,03e1h
03da 0100df    ld      bc,0df00h
03dd ed49      out     (c),c
03df 182e      jr      040fh
03e1 13        inc     de
03e2 23        inc     hl
03e3 10f1      djnz    03d6h
03e5 3eff      ld      a,0ffh
03e7 3200a3    ld      (0a300h),a
03ea 1100b4    ld      de,0b400h
03ed 21ffb8    ld      hl,0b8ffh
03f0 37        scf     
03f1 cd06c0    call    0c006h
03f4 23        inc     hl
03f5 2201a3    ld      (0a301h),hl
03f8 3e01      ld      a,01h
03fa cdea0e    call    0eeah
03fd 3eff      ld      a,0ffh
03ff fd2a01a3  ld      iy,(0a301h)
0403 cd2200    call    0022h
0406 1807      jr      040fh
0408 43        ld      b,e
0409 50        ld      d,b
040a 4d        ld      c,l
040b 2052      jr      nz,045fh
040d 4f        ld      c,a
040e cdaf32    call    32afh
0411 69        ld      l,c
0412 a3        and     e
0413 3268a3    ld      (0a368h),a
0416 326aa3    ld      (0a36ah),a
0419 326ea3    ld      (0a36eh),a
041c 323ea3    ld      (0a33eh),a
041f 329aa3    ld      (0a39ah),a
0422 327aa3    ld      (0a37ah),a
0425 0100fe    ld      bc,0fe00h
0428 ed49      out     (c),c
042a 010afc    ld      bc,0fc0ah
042d ed78      in      a,(c)
042f 323ca2    ld      (0a23ch),a
0432 cd7613    call    1376h
0435 202a      jr      nz,0461h
0437 0143fc    ld      bc,0fc43h
043a ed78      in      a,(c)
043c 3227a2    ld      (0a227h),a
043f 03        inc     bc
0440 ed78      in      a,(c)
0442 3228a2    ld      (0a228h),a
0445 03        inc     bc
0446 ed78      in      a,(c)
0448 323ea2    ld      (0a23eh),a
044b 03        inc     bc
044c 1611      ld      d,11h
044e 2129a2    ld      hl,0a229h
0451 ed78      in      a,(c)
0453 77        ld      (hl),a
0454 23        inc     hl
0455 03        inc     bc
0456 15        dec     d
0457 20f8      jr      nz,0451h
0459 ed68      in      l,(c)
045b 03        inc     bc
045c ed60      in      h,(c)
045e 223aa2    ld      (0a23ah),hl
0461 cd011a    call    1a01h
0464 cd5006    call    0650h
0467 0100fe    ld      bc,0fe00h
046a ed49      out     (c),c
046c 0140fc    ld      bc,0fc40h
046f 213b00    ld      hl,003bh
0472 1603      ld      d,03h
0474 7e        ld      a,(hl)
0475 ed79      out     (c),a
0477 23        inc     hl
0478 03        inc     bc
0479 15        dec     d
047a 20f8      jr      nz,0474h
047c 3a27a2    ld      a,(0a227h)
047f ed79      out     (c),a
0481 03        inc     bc
0482 3a28a2    ld      a,(0a228h)
0485 ed79      out     (c),a
0487 03        inc     bc
0488 3a3ea2    ld      a,(0a23eh)
048b ed79      out     (c),a
048d 1611      ld      d,11h
048f 2129a2    ld      hl,0a229h
0492 7e        ld      a,(hl)
0493 23        inc     hl
0494 03        inc     bc
0495 ed79      out     (c),a
0497 15        dec     d
0498 20f8      jr      nz,0492h
049a 03        inc     bc
049b 2a3aa2    ld      hl,(0a23ah)
049e ed69      out     (c),l
04a0 03        inc     bc
04a1 ed61      out     (c),h
04a3 cd7613    call    1376h
04a6 ed79      out     (c),a
04a8 010afc    ld      bc,0fc0ah
04ab 3a3ca2    ld      a,(0a23ch)
04ae ed79      out     (c),a
04b0 cd541c    call    1c54h
04b3 3a3ba2    ld      a,(0a23bh)
04b6 e6c0      and     0c0h
04b8 fec0      cp      0c0h
04ba f5        push    af
04bb c44a15    call    nz,154ah
04be f1        pop     af
04bf cc5d15    call    z,155dh
04c2 0101bc    ld      bc,0bc01h
04c5 ed49      out     (c),c
04c7 0100bd    ld      bc,0bd00h
04ca 3a27a2    ld      a,(0a227h)
04cd ed79      out     (c),a
04cf 0102bc    ld      bc,0bc02h
04d2 ed49      out     (c),c
04d4 0100bd    ld      bc,0bd00h
04d7 cb3f      srl     a
04d9 c61a      add     a,1ah
04db ed79      out     (c),a
04dd 0106bc    ld      bc,0bc06h
04e0 ed49      out     (c),c
04e2 0100bd    ld      bc,0bd00h
04e5 3a28a2    ld      a,(0a228h)
04e8 ed79      out     (c),a
04ea cb3f      srl     a
04ec 57        ld      d,a
04ed 3a3ea2    ld      a,(0a23eh)
04f0 82        add     a,d
04f1 57        ld      d,a
04f2 06f5      ld      b,0f5h
04f4 ed78      in      a,(c)
04f6 e610      and     10h
04f8 7a        ld      a,d
04f9 2002      jr      nz,04fdh
04fb d603      sub     03h
04fd 0107bc    ld      bc,0bc07h
0500 ed49      out     (c),c
0502 0100bd    ld      bc,0bd00h
0505 ed79      out     (c),a
0507 2a3aa2    ld      hl,(0a23ah)
050a 010dbc    ld      bc,0bc0dh
050d ed49      out     (c),c
050f 0100bd    ld      bc,0bd00h
0512 7d        ld      a,l
0513 1f        rra     
0514 ed79      out     (c),a
0516 010cbc    ld      bc,0bc0ch
0519 ed49      out     (c),c
051b 0100bd    ld      bc,0bd00h
051e 7c        ld      a,h
051f e6c7      and     0c7h
0521 67        ld      h,a
0522 e6c0      and     0c0h
0524 cb3f      srl     a
0526 57        ld      d,a
0527 7c        ld      a,h
0528 e607      and     07h
052a b2        or      d
052b cb3f      srl     a
052d ed79      out     (c),a
052f 3a9ba3    ld      a,(0a39bh)
0532 067f      ld      b,7fh
0534 ed79      out     (c),a
0536 cd321a    call    1a32h
0539 f3        di      
053a af        xor     a
053b 32febf    ld      (0bffeh),a
053e cd5d15    call    155dh
0541 3e38      ld      a,38h
0543 0e07      ld      c,07h
0545 cd4613    call    1346h
0548 0182f7    ld      bc,0f782h
054b ed49      out     (c),c
054d 010ef4    ld      bc,0f40eh
0550 ed49      out     (c),c
0552 01cff6    ld      bc,0f6cfh
0555 ed49      out     (c),c
0557 0e0f      ld      c,0fh
0559 ed49      out     (c),c
055b 0192f7    ld      bc,0f792h
055e ed49      out     (c),c
0560 3afdbf    ld      a,(0bffdh)
0563 fe40      cp      40h
0565 3048      jr      nc,05afh
0567 2afcbf    ld      hl,(0bffch)
056a 110600    ld      de,0006h
056d a7        and     a
056e ed52      sbc     hl,de
0570 e5        push    hl
0571 dde1      pop     ix
0573 dd2b      dec     ix
0575 dd2b      dec     ix
0577 7d        ld      a,l
0578 cda905    call    05a9h
057b 7c        ld      a,h
057c cda905    call    05a9h
057f 3e31      ld      a,31h
0581 cda905    call    05a9h
0584 2afcbf    ld      hl,(0bffch)
0587 7d        ld      a,l
0588 cda905    call    05a9h
058b 7c        ld      a,h
058c cda905    call    05a9h
058f 3ec1      ld      a,0c1h
0591 cda905    call    05a9h
0594 3ef1      ld      a,0f1h
0596 cda905    call    05a9h
0599 3ec9      ld      a,0c9h
059b cda905    call    05a9h
059e 110c00    ld      de,000ch
05a1 a7        and     a
05a2 ed52      sbc     hl,de
05a4 22fcbf    ld      (0bffch),hl
05a7 1806      jr      05afh
05a9 dd7700    ld      (ix+00h),a
05ac dd23      inc     ix
05ae c9        ret     
05af 2100a1    ld      hl,0a100h
05b2 cdcf19    call    19cfh
05b5 0100fe    ld      bc,0fe00h
05b8 ed49      out     (c),c
05ba 0105fc    ld      bc,0fc05h
05bd ed78      in      a,(c)
05bf e60f      and     0fh
05c1 f680      or      80h
05c3 067f      ld      b,7fh
05c5 ed79      out     (c),a
05c7 2100bf    ld      hl,0bf00h
05ca 0100fd    ld      bc,0fd00h
05cd 3eee      ld      a,0eeh
05cf ed50      in      d,(c)
05d1 72        ld      (hl),d
05d2 23        inc     hl
05d3 0c        inc     c
05d4 bd        cp      l
05d5 c2cf05    jp      nz,05cfh
05d8 fde1      pop     iy
05da dde1      pop     ix
05dc e1        pop     hl
05dd d1        pop     de
05de c1        pop     bc
05df f1        pop     af
05e0 d9        exx     
05e1 08        ex      af,af'
05e2 cd6213    call    1362h
05e5 fb        ei      
05e6 76        halt    
05e7 76        halt    
05e8 010afc    ld      bc,0fc0ah
05eb ed50      in      d,(c)
05ed af        xor     a
05ee ba        cp      d
05ef 2813      jr      z,0604h
05f1 3e0c      ld      a,0ch
05f3 92        sub     d
05f4 57        ld      d,a
05f5 01fff5    ld      bc,0f5ffh
05f8 76        halt    
05f9 ed78      in      a,(c)
05fb 1f        rra     
05fc 30fa      jr      nc,05f8h
05fe 15        dec     d
05ff 2803      jr      z,0604h
0601 76        halt    
0602 18fa      jr      05feh
0604 f1        pop     af
0605 ed47      ld      i,a
0607 ea0b06    jp      pe,060bh
060a f3        di      
060b ed7bfcbf  ld      sp,(0bffch)
060f 21eebf    ld      hl,0bfeeh
0612 01eefd    ld      bc,0fdeeh
0615 ed78      in      a,(c)
0617 77        ld      (hl),a
0618 23        inc     hl
0619 0c        inc     c
061a c21506    jp      nz,0615h
061d 0107fc    ld      bc,0fc07h
0620 ed78      in      a,(c)
0622 fe01      cp      01h
0624 2802      jr      z,0628h
0626 ed5e      im      2
0628 0d        dec     c
0629 ed48      in      c,(c)
062b 067f      ld      b,7fh
062d ed49      out     (c),c
062f 0104fc    ld      bc,0fc04h
0632 ed60      in      h,(c)
0634 0d        dec     c
0635 ed68      in      l,(c)
0637 0d        dec     c
0638 ed50      in      d,(c)
063a 0d        dec     c
063b ed58      in      e,(c)
063d 0d        dec     c
063e ed78      in      a,(c)
0640 ed4f      ld      r,a
0642 0e05      ld      c,05h
0644 ed78      in      a,(c)
0646 0100fe    ld      bc,0fe00h
0649 ed79      out     (c),a
064b c1        pop     bc
064c f1        pop     af
064d c31620    jp      2016h
0650 ef        rst     28h
0651 e7        rst     20h
0652 1c        inc     e
0653 cd1114    call    1411h
0656 ef        rst     28h
0657 64        ld      h,h
0658 1d        dec     e
0659 cd4c00    call    004ch
065c 2805      jr      z,0663h
065e f7        rst     30h
065f 8a        adc     a,d
0660 1d        dec     e
0661 1805      jr      0668h
0663 3e0d      ld      a,0dh
0665 cd6e15    call    156eh
0668 f7        rst     30h
0669 8e        adc     a,(hl)
066a 1d        dec     e
066b 3e01      ld      a,01h
066d 211a01    ld      hl,011ah
0670 cdc016    call    16c0h
0673 38f6      jr      c,066bh
0675 3a0ba3    ld      a,(0a30bh)
0678 cbaf      res     5,a
067a fe4e      cp      4eh
067c 203e      jr      nz,06bch
067e ef        rst     28h
067f 1f        rra     
0680 1e3e      ld      e,3eh
0682 01211e    ld      bc,1e21h
0685 00        nop     
0686 cdc016    call    16c0h
0689 38cb      jr      c,0656h
068b 3a0ba3    ld      a,(0a30bh)
068e fe59      cp      59h
0690 3eff      ld      a,0ffh
0692 2802      jr      z,0696h
0694 3e00      ld      a,00h
0696 32ff80    ld      (80ffh),a
0699 cd541c    call    1c54h
069c cd4a15    call    154ah
069f cdae1a    call    1aaeh
06a2 cd5815    call    1558h
06a5 3a6da3    ld      a,(0a36dh)
06a8 32fe80    ld      (80feh),a
06ab 21f41a    ld      hl,1af4h
06ae 110080    ld      de,8000h
06b1 015800    ld      bc,0058h
06b4 edb0      ldir    
06b6 210080    ld      hl,8000h
06b9 c3d11a    jp      1ad1h
06bc fe43      cp      43h
06be 200a      jr      nz,06cah
06c0 cd4c00    call    004ch
06c3 28a6      jr      z,066bh
06c5 cde511    call    11e5h
06c8 188c      jr      0656h
06ca fe52      cp      52h
06cc 2033      jr      nz,0701h
06ce cdef00    call    00efh
06d1 38f5      jr      c,06c8h
06d3 af        xor     a
06d4 323aa3    ld      (0a33ah),a
06d7 3239a3    ld      (0a339h),a
06da cd8714    call    1487h
06dd 3a39a3    ld      a,(0a339h)
06e0 a7        and     a
06e1 20f0      jr      nz,06d3h
06e3 3a3ea3    ld      a,(0a33eh)
06e6 a7        and     a
06e7 2812      jr      z,06fbh
06e9 f3        di      
06ea 2a3ca3    ld      hl,(0a33ch)
06ed 23        inc     hl
06ee 23        inc     hl
06ef 3601      ld      (hl),01h
06f1 23        inc     hl
06f2 3600      ld      (hl),00h
06f4 fb        ei      
06f5 3a3ea3    ld      a,(0a33eh)
06f8 a7        and     a
06f9 20fa      jr      nz,06f5h
06fb cd9b13    call    139bh
06fe 38c8      jr      c,06c8h
0700 c9        ret     
0701 fe53      cp      53h
0703 2010      jr      nz,0715h
0705 cdef00    call    00efh
0708 38be      jr      c,06c8h
070a 3eff      ld      a,0ffh
070c 3267a3    ld      (0a367h),a
070f cd4d07    call    074dh
0712 c3c806    jp      06c8h
0715 fe4c      cp      4ch
0717 2009      jr      nz,0722h
0719 af        xor     a
071a 3267a3    ld      (0a367h),a
071d cd4d07    call    074dh
0720 18f0      jr      0712h
0722 fe57      cp      57h
0724 200a      jr      nz,0730h
0726 cdef00    call    00efh
0729 38e7      jr      c,0712h
072b cd9c17    call    179ch
072e 18e2      jr      0712h
0730 fe49      cp      49h
0732 200a      jr      nz,073eh
0734 cdef00    call    00efh
0737 38d9      jr      c,0712h
0739 cd0718    call    1807h
073c 18d4      jr      0712h
073e fe41      cp      41h
0740 c26b06    jp      nz,066bh
0743 cdef00    call    00efh
0746 38ca      jr      c,0712h
0748 cda318    call    18a3h
074b 18c5      jr      0712h
074d cd2419    call    1924h
0750 d8        ret     c
0751 cd541c    call    1c54h
0754 cd4a15    call    154ah
0757 cd5b00    call    005bh
075a 200a      jr      nz,0766h
075c cdae1a    call    1aaeh
075f 3eff      ld      a,0ffh
0761 3269a3    ld      (0a369h),a
0764 1803      jr      0769h
0766 cd321a    call    1a32h
0769 cd9507    call    0795h
076c 01c07f    ld      bc,7fc0h
076f ed49      out     (c),c
0771 380b      jr      c,077eh
0773 cd0400    call    0004h
0776 cd011a    call    1a01h
0779 af        xor     a
077a 3269a3    ld      (0a369h),a
077d c9        ret     
077e f5        push    af
077f cd5b00    call    005bh
0782 c4011a    call    nz,1a01h
0785 cd5b00    call    005bh
0788 2006      jr      nz,0790h
078a cdae1a    call    1aaeh
078d cd5815    call    1558h
0790 f1        pop     af
0791 cdfc08    call    08fch
0794 c9        ret     
0795 cd4008    call    0840h
0798 d8        ret     c
0799 cde012    call    12e0h
079c d8        ret     c
079d cd4112    call    1241h
07a0 d8        ret     c
07a1 cd5b00    call    005bh
07a4 2806      jr      z,07ach
07a6 210000    ld      hl,0000h
07a9 cd5109    call    0951h
07ac 21ff7f    ld      hl,7fffh
07af cd0408    call    0804h
07b2 f5        push    af
07b3 210000    ld      hl,0000h
07b6 cd5109    call    0951h
07b9 f1        pop     af
07ba d8        ret     c
07bb 21ffa0    ld      hl,0a0ffh
07be cd0408    call    0804h
07c1 d8        ret     c
07c2 cdc419    call    19c4h
07c5 21ff5f    ld      hl,5fffh
07c8 cd0408    call    0804h
07cb f5        push    af
07cc cdc419    call    19c4h
07cf f1        pop     af
07d0 d8        ret     c
07d1 2100c0    ld      hl,0c000h
07d4 cd5109    call    0951h
07d7 21ff7f    ld      hl,7fffh
07da cd0408    call    0804h
07dd f5        push    af
07de 2100c0    ld      hl,0c000h
07e1 cd5109    call    0951h
07e4 f1        pop     af
07e5 d8        ret     c
07e6 3a10a2    ld      a,(0a210h)
07e9 a7        and     a
07ea 2814      jr      z,0800h
07ec 01c47f    ld      bc,7fc4h
07ef ed49      out     (c),c
07f1 c5        push    bc
07f2 21ff7f    ld      hl,7fffh
07f5 cd0408    call    0804h
07f8 c1        pop     bc
07f9 d8        ret     c
07fa 0c        inc     c
07fb 79        ld      a,c
07fc fec8      cp      0c8h
07fe 20ef      jr      nz,07efh
0800 cd7108    call    0871h
0803 c9        ret     
0804 cd5b00    call    005bh
0807 2803      jr      z,080ch
0809 cdc70a    call    0ac7h
080c 213fa3    ld      hl,0a33fh
080f 012200    ld      bc,0022h
0812 cd8708    call    0887h
0815 3810      jr      c,0827h
0817 11ff3f    ld      de,3fffh
081a 2a5fa3    ld      hl,(0a35fh)
081d a7        and     a
081e ed52      sbc     hl,de
0820 4d        ld      c,l
0821 44        ld      b,h
0822 eb        ex      de,hl
0823 23        inc     hl
0824 cd8708    call    0887h
0827 f5        push    af
0828 cd5b00    call    005bh
082b 2003      jr      nz,0830h
082d f1        pop     af
082e d8        ret     c
082f f5        push    af
0830 3a3da2    ld      a,(0a23dh)
0833 a7        and     a
0834 2805      jr      z,083bh
0836 cd410b    call    0b41h
0839 1803      jr      083eh
083b cd9809    call    0998h
083e f1        pop     af
083f c9        ret     
0840 cd5600    call    0056h
0843 2817      jr      z,085ch
0845 060c      ld      b,0ch
0847 212da3    ld      hl,0a32dh
084a 1100ac    ld      de,0ac00h
084d cd5b00    call    005bh
0850 2805      jr      z,0857h
0852 cd3a0f    call    0f3ah
0855 1809      jr      0860h
0857 cd090f    call    0f09h
085a 1804      jr      0860h
085c cd9d0b    call    0b9dh
085f 3f        ccf     
0860 3002      jr      nc,0864h
0862 a7        and     a
0863 c9        ret     
0864 f5        push    af
0865 cd5600    call    0056h
0868 2004      jr      nz,086eh
086a f1        pop     af
086b c3c308    jp      08c3h
086e f1        pop     af
086f 37        scf     
0870 c9        ret     
0871 cd5600    call    0056h
0874 c0        ret     nz
0875 cd5b00    call    005bh
0878 2804      jr      z,087eh
087a cd260d    call    0d26h
087d c9        ret     
087e cd6000    call    0060h
0881 c8        ret     z
0882 cd7abc    call    0bc7ah
0885 3f        ccf     
0886 c9        ret     
0887 cd5600    call    0056h
088a 204d      jr      nz,08d9h
088c c5        push    bc
088d e5        push    hl
088e cdf013    call    13f0h
0891 e1        pop     hl
0892 c1        pop     bc
0893 382e      jr      c,08c3h
0895 cd5b00    call    005bh
0898 2038      jr      nz,08d2h
089a cd6000    call    0060h
089d 2002      jr      nz,08a1h
089f 1831      jr      08d2h
08a1 04        inc     b
08a2 fd2a01a3  ld      iy,(0a301h)
08a6 cd80bc    call    0bc80h
08a9 dab208    jp      c,08b2h
08ac 2815      jr      z,08c3h
08ae fe1a      cp      1ah
08b0 2011      jr      nz,08c3h
08b2 77        ld      (hl),a
08b3 23        inc     hl
08b4 0d        dec     c
08b5 c2a608    jp      nz,08a6h
08b8 05        dec     b
08b9 c2a608    jp      nz,08a6h
08bc cdf013    call    13f0h
08bf 3802      jr      c,08c3h
08c1 a7        and     a
08c2 c9        ret     
08c3 f5        push    af
08c4 cd5b00    call    005bh
08c7 2006      jr      nz,08cfh
08c9 cd6000    call    0060h
08cc c47dbc    call    nz,0bc7dh
08cf f1        pop     af
08d0 37        scf     
08d1 c9        ret     
08d2 cda10d    call    0da1h
08d5 38ec      jr      c,08c3h
08d7 18e3      jr      08bch
08d9 c5        push    bc
08da d1        pop     de
08db cd5b00    call    005bh
08de 3e16      ld      a,16h
08e0 280d      jr      z,08efh
08e2 cda00f    call    0fa0h
08e5 3f        ccf     
08e6 d0        ret     nc
08e7 3d        dec     a
08e8 feff      cp      0ffh
08ea 37        scf     
08eb c8        ret     z
08ec 3efc      ld      a,0fch
08ee c9        ret     
08ef cd970f    call    0f97h
08f2 3f        ccf     
08f3 d0        ret     nc
08f4 3d        dec     a
08f5 feff      cp      0ffh
08f7 37        scf     
08f8 c8        ret     z
08f9 3efb      ld      a,0fbh
08fb c9        ret     
08fc feff      cp      0ffh
08fe 2831      jr      z,0931h
0900 f5        push    af
0901 fefe      cp      0feh
0903 cc7108    call    z,0871h
0906 cd8a1a    call    1a8ah
0909 f1        pop     af
090a 0608      ld      b,08h
090c dd213309  ld      ix,0933h
0910 ddbe00    cp      (ix+00h)
0913 280d      jr      z,0922h
0915 dd23      inc     ix
0917 dd23      inc     ix
0919 dd23      inc     ix
091b 10f3      djnz    0910h
091d 21011f    ld      hl,1f01h
0920 1806      jr      0928h
0922 dd6e01    ld      l,(ix+01h)
0925 dd6602    ld      h,(ix+02h)
0928 cdc71a    call    1ac7h
092b f7        rst     30h
092c 0e1f      ld      c,1fh
092e cd1114    call    1411h
0931 37        scf     
0932 c9        ret     
0933 fe72      cp      72h
0935 1f        rra     
0936 fdb5      or      iyl
0938 1f        rra     
0939 fca21f    call    m,1fa2h
093c fb        ei      
093d 5e        ld      e,(hl)
093e 1f        rra     
093f 90        sub     b
0940 83        add     a,e
0941 1f        rra     
0942 92        sub     d
0943 2a1f94    ld      hl,(941fh)
0946 53        ld      d,e
0947 1f        rra     
0948 c23a1f    jp      nz,1f3ah
094b f3        di      
094c 1100c0    ld      de,0c000h
094f 1807      jr      0958h
0951 010040    ld      bc,4000h
0954 f3        di      
0955 110040    ld      de,4000h
0958 c5        push    bc
0959 d5        push    de
095a e5        push    hl
095b 018d7f    ld      bc,7f8dh
095e ed49      out     (c),c
0960 217809    ld      hl,0978h
0963 1100a1    ld      de,0a100h
0966 012000    ld      bc,0020h
0969 edb0      ldir    
096b e1        pop     hl
096c d1        pop     de
096d c1        pop     bc
096e cd00a1    call    0a100h
0971 01857f    ld      bc,7f85h
0974 ed49      out     (c),c
0976 fb        ei      
0977 c9        ret     
0978 cdfb3f    call    3ffbh
097b 7e        ld      a,(hl)
097c 08        ex      af,af'
097d 1a        ld      a,(de)
097e 77        ld      (hl),a
097f 08        ex      af,af'
0980 12        ld      (de),a
0981 23        inc     hl
0982 13        inc     de
0983 0b        dec     bc
0984 78        ld      a,b
0985 b1        or      c
0986 c203a1    jp      nz,0a103h
0989 3afb3f    ld      a,(3ffbh)
098c 21fb3f    ld      hl,3ffbh
098f 36c9      ld      (hl),0c9h
0991 cdfb3f    call    3ffbh
0994 32fb3f    ld      (3ffbh),a
0997 c9        ret     
0998 f3        di      
0999 215fa3    ld      hl,0a35fh
099c 0608      ld      b,08h
099e 2b        dec     hl
099f 56        ld      d,(hl)
09a0 2b        dec     hl
09a1 5e        ld      e,(hl)
09a2 ed5365a3  ld      (0a365h),de
09a6 2b        dec     hl
09a7 56        ld      d,(hl)
09a8 2b        dec     hl
09a9 5e        ld      e,(hl)
09aa ed5363a3  ld      (0a363h),de
09ae ed5b65a3  ld      de,(0a365h)
09b2 7b        ld      a,e
09b3 b2        or      d
09b4 2807      jr      z,09bdh
09b6 e5        push    hl
09b7 c5        push    bc
09b8 cdc109    call    09c1h
09bb c1        pop     bc
09bc e1        pop     hl
09bd 10df      djnz    099eh
09bf fb        ei      
09c0 c9        ret     
09c1 ed4b65a3  ld      bc,(0a365h)
09c5 0b        dec     bc
09c6 2a5fa3    ld      hl,(0a35fh)
09c9 09        add     hl,bc
09ca eb        ex      de,hl
09cb 2a5fa3    ld      hl,(0a35fh)
09ce ed535fa3  ld      (0a35fh),de
09d2 ed4b63a3  ld      bc,(0a363h)
09d6 e5        push    hl
09d7 a7        and     a
09d8 ed42      sbc     hl,bc
09da 4d        ld      c,l
09db 44        ld      b,h
09dc e1        pop     hl
09dd edb8      lddr    
09df 2a63a3    ld      hl,(0a363h)
09e2 56        ld      d,(hl)
09e3 23        inc     hl
09e4 ed4b65a3  ld      bc,(0a365h)
09e8 0b        dec     bc
09e9 72        ld      (hl),d
09ea 23        inc     hl
09eb 0b        dec     bc
09ec 78        ld      a,b
09ed b1        or      c
09ee 20f9      jr      nz,09e9h
09f0 c9        ret     
09f1 0ec0      ld      c,0c0h
09f3 cd700a    call    0a70h
09f6 201e      jr      nz,0a16h
09f8 cd680a    call    0a68h
09fb 2829      jr      z,0a26h
09fd 0ec4      ld      c,0c4h
09ff cd680a    call    0a68h
0a02 2822      jr      z,0a26h
0a04 0ec5      ld      c,0c5h
0a06 cd680a    call    0a68h
0a09 281b      jr      z,0a26h
0a0b 0ec6      ld      c,0c6h
0a0d cd680a    call    0a68h
0a10 2814      jr      z,0a26h
0a12 0ec7      ld      c,0c7h
0a14 1810      jr      0a26h
0a16 0ec1      ld      c,0c1h
0a18 cd680a    call    0a68h
0a1b 2809      jr      z,0a26h
0a1d 0ec3      ld      c,0c3h
0a1f cd680a    call    0a68h
0a22 2802      jr      z,0a26h
0a24 0ec2      ld      c,0c2h
0a26 79        ld      a,c
0a27 0106fc    ld      bc,0fc06h
0a2a ed79      out     (c),a
0a2c 5f        ld      e,a
0a2d 067f      ld      b,7fh
0a2f fec2      cp      0c2h
0a31 2002      jr      nz,0a35h
0a33 3ec5      ld      a,0c5h
0a35 ed79      out     (c),a
0a37 010cfc    ld      bc,0fc0ch
0a3a 210040    ld      hl,4000h
0a3d 1604      ld      d,04h
0a3f ed78      in      a,(c)
0a41 77        ld      (hl),a
0a42 0c        inc     c
0a43 23        inc     hl
0a44 15        dec     d
0a45 20f8      jr      nz,0a3fh
0a47 c5        push    bc
0a48 01ff7f    ld      bc,7fffh
0a4b 7b        ld      a,e
0a4c fec2      cp      0c2h
0a4e 2002      jr      nz,0a52h
0a50 3ec1      ld      a,0c1h
0a52 ed79      out     (c),a
0a54 c1        pop     bc
0a55 2100c0    ld      hl,0c000h
0a58 1604      ld      d,04h
0a5a ed78      in      a,(c)
0a5c 77        ld      (hl),a
0a5d 0c        inc     c
0a5e 23        inc     hl
0a5f 15        dec     d
0a60 20f8      jr      nz,0a5ah
0a62 01c07f    ld      bc,7fc0h
0a65 ed49      out     (c),c
0a67 c9        ret     
0a68 213b00    ld      hl,003bh
0a6b 110040    ld      de,4000h
0a6e 1806      jr      0a76h
0a70 213f00    ld      hl,003fh
0a73 1100c0    ld      de,0c000h
0a76 067f      ld      b,7fh
0a78 ed49      out     (c),c
0a7a 0604      ld      b,04h
0a7c 1a        ld      a,(de)
0a7d be        cp      (hl)
0a7e c0        ret     nz
0a7f 13        inc     de
0a80 23        inc     hl
0a81 10f9      djnz    0a7ch
0a83 c9        ret     
0a84 af        xor     a
0a85 326da3    ld      (0a36dh),a
0a88 01c07f    ld      bc,7fc0h
0a8b ed49      out     (c),c
0a8d 3a0040    ld      a,(4000h)
0a90 57        ld      d,a
0a91 3eaa      ld      a,0aah
0a93 320040    ld      (4000h),a
0a96 01c47f    ld      bc,7fc4h
0a99 ed49      out     (c),c
0a9b 3a0040    ld      a,(4000h)
0a9e 5f        ld      e,a
0a9f 3e55      ld      a,55h
0aa1 320040    ld      (4000h),a
0aa4 01c07f    ld      bc,7fc0h
0aa7 ed49      out     (c),c
0aa9 3a0040    ld      a,(4000h)
0aac fe55      cp      55h
0aae 7a        ld      a,d
0aaf 320040    ld      (4000h),a
0ab2 c8        ret     z
0ab3 3e01      ld      a,01h
0ab5 326da3    ld      (0a36dh),a
0ab8 01c47f    ld      bc,7fc4h
0abb ed49      out     (c),c
0abd 7b        ld      a,e
0abe 320040    ld      (4000h),a
0ac1 01c07f    ld      bc,7fc0h
0ac4 ed49      out     (c),c
0ac6 c9        ret     
0ac7 f3        di      
0ac8 225fa3    ld      (0a35fh),hl
0acb 54        ld      d,h
0acc 210040    ld      hl,4000h
0acf 010000    ld      bc,0000h
0ad2 ed4361a3  ld      (0a361h),bc
0ad6 7a        ld      a,d
0ad7 bc        cp      h
0ad8 c2ea0a    jp      nz,0aeah
0adb 3a5fa3    ld      a,(0a35fh)
0ade bd        cp      l
0adf c2ea0a    jp      nz,0aeah
0ae2 2a61a3    ld      hl,(0a361h)
0ae5 223fa3    ld      (0a33fh),hl
0ae8 fb        ei      
0ae9 c9        ret     
0aea 010000    ld      bc,0000h
0aed 2263a3    ld      (0a363h),hl
0af0 5e        ld      e,(hl)
0af1 7e        ld      a,(hl)
0af2 bb        cp      e
0af3 200e      jr      nz,0b03h
0af5 03        inc     bc
0af6 23        inc     hl
0af7 7a        ld      a,d
0af8 bc        cp      h
0af9 c2f10a    jp      nz,0af1h
0afc 3a5fa3    ld      a,(0a35fh)
0aff bd        cp      l
0b00 c2f10a    jp      nz,0af1h
0b03 78        ld      a,b
0b04 a7        and     a
0b05 2006      jr      nz,0b0dh
0b07 3e14      ld      a,14h
0b09 b9        cp      c
0b0a d2d60a    jp      nc,0ad6h
0b0d e5        push    hl
0b0e 2a63a3    ld      hl,(0a363h)
0b11 ed5b61a3  ld      de,(0a361h)
0b15 73        ld      (hl),e
0b16 23        inc     hl
0b17 72        ld      (hl),d
0b18 23        inc     hl
0b19 71        ld      (hl),c
0b1a 23        inc     hl
0b1b 70        ld      (hl),b
0b1c d1        pop     de
0b1d 2a5fa3    ld      hl,(0a35fh)
0b20 a7        and     a
0b21 ed52      sbc     hl,de
0b23 23        inc     hl
0b24 4d        ld      c,l
0b25 44        ld      b,h
0b26 2a63a3    ld      hl,(0a363h)
0b29 23        inc     hl
0b2a 23        inc     hl
0b2b 23        inc     hl
0b2c 23        inc     hl
0b2d 23        inc     hl
0b2e e5        push    hl
0b2f eb        ex      de,hl
0b30 edb0      ldir    
0b32 1b        dec     de
0b33 ed535fa3  ld      (0a35fh),de
0b37 2a63a3    ld      hl,(0a363h)
0b3a 2261a3    ld      (0a361h),hl
0b3d e1        pop     hl
0b3e c3d60a    jp      0ad6h
0b41 f3        di      
0b42 2a3fa3    ld      hl,(0a33fh)
0b45 2261a3    ld      (0a361h),hl
0b48 2a61a3    ld      hl,(0a361h)
0b4b 7c        ld      a,h
0b4c b5        or      l
0b4d c2520b    jp      nz,0b52h
0b50 fb        ei      
0b51 c9        ret     
0b52 2263a3    ld      (0a363h),hl
0b55 5e        ld      e,(hl)
0b56 23        inc     hl
0b57 56        ld      d,(hl)
0b58 ed5361a3  ld      (0a361h),de
0b5c 23        inc     hl
0b5d 4e        ld      c,(hl)
0b5e 23        inc     hl
0b5f 46        ld      b,(hl)
0b60 ed4365a3  ld      (0a365h),bc
0b64 23        inc     hl
0b65 7e        ld      a,(hl)
0b66 323fa3    ld      (0a33fh),a
0b69 0b        dec     bc
0b6a 0b        dec     bc
0b6b 0b        dec     bc
0b6c 0b        dec     bc
0b6d 0b        dec     bc
0b6e 2a5fa3    ld      hl,(0a35fh)
0b71 09        add     hl,bc
0b72 eb        ex      de,hl
0b73 2a5fa3    ld      hl,(0a35fh)
0b76 ed535fa3  ld      (0a35fh),de
0b7a ed4b63a3  ld      bc,(0a363h)
0b7e e5        push    hl
0b7f a7        and     a
0b80 ed42      sbc     hl,bc
0b82 4d        ld      c,l
0b83 44        ld      b,h
0b84 e1        pop     hl
0b85 edb8      lddr    
0b87 2a63a3    ld      hl,(0a363h)
0b8a 3a3fa3    ld      a,(0a33fh)
0b8d 57        ld      d,a
0b8e ed4b65a3  ld      bc,(0a365h)
0b92 72        ld      (hl),d
0b93 23        inc     hl
0b94 0b        dec     bc
0b95 78        ld      a,b
0b96 b1        or      c
0b97 c2920b    jp      nz,0b92h
0b9a c3480b    jp      0b48h
0b9d 3a2ba3    ld      a,(0a32bh)
0ba0 cdea0e    call    0eeah
0ba3 af        xor     a
0ba4 328fa3    ld      (0a38fh),a
0ba7 328ea3    ld      (0a38eh),a
0baa 3291a3    ld      (0a391h),a
0bad 3283a3    ld      (0a383h),a
0bb0 fd2a01a3  ld      iy,(0a301h)
0bb4 cd2200    call    0022h
0bb7 3e04      ld      a,04h
0bb9 cdea0e    call    0eeah
0bbc dd2288a3  ld      (0a388h),ix
0bc0 3e05      ld      a,05h
0bc2 cdea0e    call    0eeah
0bc5 dd228aa3  ld      (0a38ah),ix
0bc9 2a40be    ld      hl,(0be40h)
0bcc 110a00    ld      de,000ah
0bcf 3a2ba3    ld      a,(0a32bh)
0bd2 fe41      cp      41h
0bd4 2803      jr      z,0bd9h
0bd6 111a00    ld      de,001ah
0bd9 19        add     hl,de
0bda 5e        ld      e,(hl)
0bdb 23        inc     hl
0bdc 56        ld      d,(hl)
0bdd 210d00    ld      hl,000dh
0be0 19        add     hl,de
0be1 7e        ld      a,(hl)
0be2 328ca3    ld      (0a38ch),a
0be5 06b4      ld      b,0b4h
0be7 a7        and     a
0be8 2802      jr      z,0bech
0bea 06ab      ld      b,0abh
0bec 23        inc     hl
0bed 23        inc     hl
0bee 7e        ld      a,(hl)
0bef 328da3    ld      (0a38dh),a
0bf2 2100b0    ld      hl,0b000h
0bf5 3600      ld      (hl),00h
0bf7 23        inc     hl
0bf8 3600      ld      (hl),00h
0bfa 23        inc     hl
0bfb 3e02      ld      a,02h
0bfd 77        ld      (hl),a
0bfe 23        inc     hl
0bff 3c        inc     a
0c00 b8        cp      b
0c01 20fa      jr      nz,0bfdh
0c03 3601      ld      (hl),01h
0c05 af        xor     a
0c06 3282a3    ld      (0a382h),a
0c09 2100ac    ld      hl,0ac00h
0c0c 47        ld      b,a
0c0d af        xor     a
0c0e 3290a3    ld      (0a390h),a
0c11 78        ld      a,b
0c12 cd760e    call    0e76h
0c15 d8        ret     c
0c16 0620      ld      b,20h
0c18 2100ac    ld      hl,0ac00h
0c1b e5        push    hl
0c1c 7e        ld      a,(hl)
0c1d fe00      cp      00h
0c1f 2045      jr      nz,0c66h
0c21 0e08      ld      c,08h
0c23 112ca3    ld      de,0a32ch
0c26 cd1d0d    call    0d1dh
0c29 203b      jr      nz,0c66h
0c2b 0e03      ld      c,03h
0c2d 113300    ld      de,0033h
0c30 cd1d0d    call    0d1dh
0c33 2031      jr      nz,0c66h
0c35 cd5b00    call    005bh
0c38 2806      jr      z,0c40h
0c3a e1        pop     hl
0c3b e5        push    hl
0c3c 36e5      ld      (hl),0e5h
0c3e 1826      jr      0c66h
0c40 3eff      ld      a,0ffh
0c42 328fa3    ld      (0a38fh),a
0c45 23        inc     hl
0c46 1600      ld      d,00h
0c48 5e        ld      e,(hl)
0c49 cb23      sla     e
0c4b cb23      sla     e
0c4d cb23      sla     e
0c4f cb23      sla     e
0c51 23        inc     hl
0c52 23        inc     hl
0c53 23        inc     hl
0c54 23        inc     hl
0c55 e5        push    hl
0c56 2100b0    ld      hl,0b000h
0c59 19        add     hl,de
0c5a d1        pop     de
0c5b 0e10      ld      c,10h
0c5d 1a        ld      a,(de)
0c5e 77        ld      (hl),a
0c5f 23        inc     hl
0c60 13        inc     de
0c61 0d        dec     c
0c62 20f9      jr      nz,0c5dh
0c64 1833      jr      0c99h
0c66 cd5b00    call    005bh
0c69 282e      jr      z,0c99h
0c6b e1        pop     hl
0c6c e5        push    hl
0c6d 7e        ld      a,(hl)
0c6e fee5      cp      0e5h
0c70 2009      jr      nz,0c7bh
0c72 3a91a3    ld      a,(0a391h)
0c75 a7        and     a
0c76 c4420d    call    nz,0d42h
0c79 181e      jr      0c99h
0c7b 110f00    ld      de,000fh
0c7e 19        add     hl,de
0c7f 7e        ld      a,(hl)
0c80 c607      add     a,07h
0c82 cb3f      srl     a
0c84 cb3f      srl     a
0c86 cb3f      srl     a
0c88 4f        ld      c,a
0c89 23        inc     hl
0c8a e5        push    hl
0c8b 1600      ld      d,00h
0c8d 5e        ld      e,(hl)
0c8e 2100b0    ld      hl,0b000h
0c91 19        add     hl,de
0c92 3600      ld      (hl),00h
0c94 e1        pop     hl
0c95 23        inc     hl
0c96 0d        dec     c
0c97 20f1      jr      nz,0c8ah
0c99 e1        pop     hl
0c9a 112000    ld      de,0020h
0c9d 19        add     hl,de
0c9e 05        dec     b
0c9f c21b0c    jp      nz,0c1bh
0ca2 cd5b00    call    005bh
0ca5 280f      jr      z,0cb6h
0ca7 3eff      ld      a,0ffh
0ca9 3290a3    ld      (0a390h),a
0cac 3a82a3    ld      a,(0a382h)
0caf 2100ac    ld      hl,0ac00h
0cb2 cd760e    call    0e76h
0cb5 d8        ret     c
0cb6 3a82a3    ld      a,(0a382h)
0cb9 3c        inc     a
0cba 3282a3    ld      (0a382h),a
0cbd fe02      cp      02h
0cbf c2090c    jp      nz,0c09h
0cc2 cd5b00    call    005bh
0cc5 2807      jr      z,0cceh
0cc7 3a91a3    ld      a,(0a391h)
0cca a7        and     a
0ccb c0        ret     nz
0ccc 180a      jr      0cd8h
0cce 3a8fa3    ld      a,(0a38fh)
0cd1 a7        and     a
0cd2 2004      jr      nz,0cd8h
0cd4 3e92      ld      a,92h
0cd6 37        scf     
0cd7 c9        ret     
0cd8 2100b0    ld      hl,0b000h
0cdb 227fa3    ld      (0a37fh),hl
0cde af        xor     a
0cdf 3281a3    ld      (0a381h),a
0ce2 cd5b00    call    005bh
0ce5 2007      jr      nz,0ceeh
0ce7 2100ac    ld      hl,0ac00h
0cea cd5a0e    call    0e5ah
0ced d8        ret     c
0cee 2180ac    ld      hl,0ac80h
0cf1 227ba3    ld      (0a37bh),hl
0cf4 218003    ld      hl,0380h
0cf7 227da3    ld      (0a37dh),hl
0cfa 2100ac    ld      hl,0ac00h
0cfd cd5b00    call    005bh
0d00 c0        ret     nz
0d01 3a12ac    ld      a,(0ac12h)
0d04 d602      sub     02h
0d06 328ea3    ld      (0a38eh),a
0d09 cd6000    call    0060h
0d0c 2002      jr      nz,0d10h
0d0e a7        and     a
0d0f c9        ret     
0d10 060e      ld      b,0eh
0d12 212ba3    ld      hl,0a32bh
0d15 1100ac    ld      de,0ac00h
0d18 cd77bc    call    0bc77h
0d1b 3f        ccf     
0d1c c9        ret     
0d1d 23        inc     hl
0d1e 13        inc     de
0d1f 1a        ld      a,(de)
0d20 be        cp      (hl)
0d21 c0        ret     nz
0d22 0d        dec     c
0d23 20f8      jr      nz,0d1dh
0d25 c9        ret     
0d26 2a7da3    ld      hl,(0a37dh)
0d29 7c        ld      a,h
0d2a b5        or      l
0d2b 2807      jr      z,0d34h
0d2d 2100ac    ld      hl,0ac00h
0d30 cd5a0e    call    0e5ah
0d33 d8        ret     c
0d34 3eff      ld      a,0ffh
0d36 3291a3    ld      (0a391h),a
0d39 2100b0    ld      hl,0b000h
0d3c 227fa3    ld      (0a37fh),hl
0d3f c3050c    jp      0c05h
0d42 3a81a3    ld      a,(0a381h)
0d45 a7        and     a
0d46 c8        ret     z
0d47 3600      ld      (hl),00h
0d49 23        inc     hl
0d4a c5        push    bc
0d4b eb        ex      de,hl
0d4c 212da3    ld      hl,0a32dh
0d4f 010800    ld      bc,0008h
0d52 edb0      ldir    
0d54 213400    ld      hl,0034h
0d57 010300    ld      bc,0003h
0d5a edb0      ldir    
0d5c eb        ex      de,hl
0d5d c1        pop     bc
0d5e 3a83a3    ld      a,(0a383h)
0d61 77        ld      (hl),a
0d62 3c        inc     a
0d63 3283a3    ld      (0a383h),a
0d66 23        inc     hl
0d67 3600      ld      (hl),00h
0d69 23        inc     hl
0d6a 3600      ld      (hl),00h
0d6c 23        inc     hl
0d6d 3a81a3    ld      a,(0a381h)
0d70 0e10      ld      c,10h
0d72 91        sub     c
0d73 3003      jr      nc,0d78h
0d75 81        add     a,c
0d76 4f        ld      c,a
0d77 af        xor     a
0d78 3281a3    ld      (0a381h),a
0d7b 79        ld      a,c
0d7c cb27      sla     a
0d7e cb27      sla     a
0d80 cb27      sla     a
0d82 77        ld      (hl),a
0d83 23        inc     hl
0d84 e5        push    hl
0d85 3e10      ld      a,10h
0d87 3600      ld      (hl),00h
0d89 23        inc     hl
0d8a 3d        dec     a
0d8b 20fa      jr      nz,0d87h
0d8d e1        pop     hl
0d8e ed5b7fa3  ld      de,(0a37fh)
0d92 1a        ld      a,(de)
0d93 13        inc     de
0d94 a7        and     a
0d95 28fb      jr      z,0d92h
0d97 77        ld      (hl),a
0d98 23        inc     hl
0d99 0d        dec     c
0d9a 20f6      jr      nz,0d92h
0d9c ed537fa3  ld      (0a37fh),de
0da0 c9        ret     
0da1 2284a3    ld      (0a384h),hl
0da4 ed4386a3  ld      (0a386h),bc
0da8 ed5b86a3  ld      de,(0a386h)
0dac 7a        ld      a,d
0dad b3        or      e
0dae c8        ret     z
0daf 2a7da3    ld      hl,(0a37dh)
0db2 7c        ld      a,h
0db3 b5        or      l
0db4 2039      jr      nz,0defh
0db6 eb        ex      de,hl
0db7 110004    ld      de,0400h
0dba a7        and     a
0dbb ed52      sbc     hl,de
0dbd 301a      jr      nc,0dd9h
0dbf cd5b00    call    005bh
0dc2 2007      jr      nz,0dcbh
0dc4 2100ac    ld      hl,0ac00h
0dc7 cd5a0e    call    0e5ah
0dca d8        ret     c
0dcb 210004    ld      hl,0400h
0dce 227da3    ld      (0a37dh),hl
0dd1 2100ac    ld      hl,0ac00h
0dd4 227ba3    ld      (0a37bh),hl
0dd7 18cf      jr      0da8h
0dd9 2286a3    ld      (0a386h),hl
0ddc 2a84a3    ld      hl,(0a384h)
0ddf cd5a0e    call    0e5ah
0de2 d8        ret     c
0de3 2a84a3    ld      hl,(0a384h)
0de6 110004    ld      de,0400h
0de9 19        add     hl,de
0dea 2284a3    ld      (0a384h),hl
0ded 18b9      jr      0da8h
0def a7        and     a
0df0 ed52      sbc     hl,de
0df2 3040      jr      nc,0e34h
0df4 2a7ba3    ld      hl,(0a37bh)
0df7 ed5b84a3  ld      de,(0a384h)
0dfb ed4b7da3  ld      bc,(0a37dh)
0dff cd5b00    call    005bh
0e02 2801      jr      z,0e05h
0e04 eb        ex      de,hl
0e05 edb0      ldir    
0e07 cd5b00    call    005bh
0e0a 2801      jr      z,0e0dh
0e0c eb        ex      de,hl
0e0d ed5384a3  ld      (0a384h),de
0e11 2a86a3    ld      hl,(0a386h)
0e14 ed5b7da3  ld      de,(0a37dh)
0e18 a7        and     a
0e19 ed52      sbc     hl,de
0e1b 2286a3    ld      (0a386h),hl
0e1e 210000    ld      hl,0000h
0e21 227da3    ld      (0a37dh),hl
0e24 cd5b00    call    005bh
0e27 caa80d    jp      z,0da8h
0e2a 2100ac    ld      hl,0ac00h
0e2d cd5a0e    call    0e5ah
0e30 d8        ret     c
0e31 c3a80d    jp      0da8h
0e34 227da3    ld      (0a37dh),hl
0e37 2a7ba3    ld      hl,(0a37bh)
0e3a ed5b84a3  ld      de,(0a384h)
0e3e ed4b86a3  ld      bc,(0a386h)
0e42 cd5b00    call    005bh
0e45 2801      jr      z,0e48h
0e47 eb        ex      de,hl
0e48 edb0      ldir    
0e4a cd5b00    call    005bh
0e4d 2801      jr      z,0e50h
0e4f eb        ex      de,hl
0e50 227ba3    ld      (0a37bh),hl
0e53 ed4386a3  ld      (0a386h),bc
0e57 c3a80d    jp      0da8h
0e5a 3a81a3    ld      a,(0a381h)
0e5d 3c        inc     a
0e5e 3281a3    ld      (0a381h),a
0e61 ed5b7fa3  ld      de,(0a37fh)
0e65 1a        ld      a,(de)
0e66 13        inc     de
0e67 a7        and     a
0e68 28fb      jr      z,0e65h
0e6a ed537fa3  ld      (0a37fh),de
0e6e fe01      cp      01h
0e70 2004      jr      nz,0e76h
0e72 3e94      ld      a,94h
0e74 37        scf     
0e75 c9        ret     
0e76 e5        push    hl
0e77 010900    ld      bc,0009h
0e7a 2600      ld      h,00h
0e7c 6f        ld      l,a
0e7d cb25      sla     l
0e7f cb14      rl      h
0e81 1600      ld      d,00h
0e83 a7        and     a
0e84 ed42      sbc     hl,bc
0e86 3803      jr      c,0e8bh
0e88 14        inc     d
0e89 18f8      jr      0e83h
0e8b 09        add     hl,bc
0e8c 4d        ld      c,l
0e8d e1        pop     hl
0e8e 3a2ba3    ld      a,(0a32bh)
0e91 d641      sub     41h
0e93 5f        ld      e,a
0e94 cdab0e    call    0eabh
0e97 d8        ret     c
0e98 d5        push    de
0e99 110002    ld      de,0200h
0e9c 19        add     hl,de
0e9d d1        pop     de
0e9e 0c        inc     c
0e9f 79        ld      a,c
0ea0 fe09      cp      09h
0ea2 2003      jr      nz,0ea7h
0ea4 0e00      ld      c,00h
0ea6 14        inc     d
0ea7 cdab0e    call    0eabh
0eaa c9        ret     
0eab c5        push    bc
0eac d5        push    de
0ead 3a8ca3    ld      a,(0a38ch)
0eb0 82        add     a,d
0eb1 57        ld      d,a
0eb2 3a8da3    ld      a,(0a38dh)
0eb5 81        add     a,c
0eb6 4f        ld      c,a
0eb7 fd2a01a3  ld      iy,(0a301h)
0ebb dd2a88a3  ld      ix,(0a388h)
0ebf 3a90a3    ld      a,(0a390h)
0ec2 a7        and     a
0ec3 2804      jr      z,0ec9h
0ec5 dd2a8aa3  ld      ix,(0a38ah)
0ec9 cd2200    call    0022h
0ecc 3f        ccf     
0ecd 3018      jr      nc,0ee7h
0ecf 47        ld      b,a
0ed0 3e90      ld      a,90h
0ed2 cb58      bit     3,b
0ed4 2011      jr      nz,0ee7h
0ed6 3e00      ld      a,00h
0ed8 cb48      bit     1,b
0eda 280b      jr      z,0ee7h
0edc 3a90a3    ld      a,(0a390h)
0edf a7        and     a
0ee0 3ec2      ld      a,0c2h
0ee2 37        scf     
0ee3 2002      jr      nz,0ee7h
0ee5 3e00      ld      a,00h
0ee7 d1        pop     de
0ee8 c1        pop     bc
0ee9 c9        ret     
0eea f680      or      80h
0eec 2a04c0    ld      hl,(0c004h)
0eef 0600      ld      b,00h
0ef1 4e        ld      c,(hl)
0ef2 b9        cp      c
0ef3 2808      jr      z,0efdh
0ef5 cb79      bit     7,c
0ef7 2801      jr      z,0efah
0ef9 04        inc     b
0efa 23        inc     hl
0efb 18f4      jr      0ef1h
0efd dd2106c0  ld      ix,0c006h
0f01 110300    ld      de,0003h
0f04 dd19      add     ix,de
0f06 10fc      djnz    0f04h
0f08 c9        ret     
0f09 c5        push    bc
0f0a d5        push    de
0f0b e5        push    hl
0f0c cd7c0f    call    0f7ch
0f0f 62        ld      h,d
0f10 6b        ld      l,e
0f11 114000    ld      de,0040h
0f14 3e2c      ld      a,2ch
0f16 cd970f    call    0f97h
0f19 e1        pop     hl
0f1a d1        pop     de
0f1b c1        pop     bc
0f1c 3807      jr      c,0f25h
0f1e a7        and     a
0f1f 20e8      jr      nz,0f09h
0f21 3eff      ld      a,0ffh
0f23 a7        and     a
0f24 c9        ret     
0f25 c5        push    bc
0f26 d5        push    de
0f27 e5        push    hl
0f28 1a        ld      a,(de)
0f29 be        cp      (hl)
0f2a 2009      jr      nz,0f35h
0f2c 23        inc     hl
0f2d 13        inc     de
0f2e 10f8      djnz    0f28h
0f30 d1        pop     de
0f31 e1        pop     hl
0f32 c1        pop     bc
0f33 37        scf     
0f34 c9        ret     
0f35 e1        pop     hl
0f36 d1        pop     de
0f37 c1        pop     bc
0f38 18cf      jr      0f09h
0f3a d5        push    de
0f3b c5        push    bc
0f3c e5        push    hl
0f3d 62        ld      h,d
0f3e 6b        ld      l,e
0f3f 0640      ld      b,40h
0f41 3600      ld      (hl),00h
0f43 23        inc     hl
0f44 10fb      djnz    0f41h
0f46 e1        pop     hl
0f47 c1        pop     bc
0f48 48        ld      c,b
0f49 0600      ld      b,00h
0f4b edb0      ldir    
0f4d e1        pop     hl
0f4e e5        push    hl
0f4f 111000    ld      de,0010h
0f52 19        add     hl,de
0f53 3601      ld      (hl),01h
0f55 23        inc     hl
0f56 36ff      ld      (hl),0ffh
0f58 23        inc     hl
0f59 3602      ld      (hl),02h
0f5b 110500    ld      de,0005h
0f5e 19        add     hl,de
0f5f 36ff      ld      (hl),0ffh
0f61 cd7c0f    call    0f7ch
0f64 e1        pop     hl
0f65 37        scf     
0f66 c9        ret     
0f67 2100ac    ld      hl,0ac00h
0f6a 114000    ld      de,0040h
0f6d 3e2c      ld      a,2ch
0f6f cda00f    call    0fa0h
0f72 3f        ccf     
0f73 d0        ret     nc
0f74 3d        dec     a
0f75 feff      cp      0ffh
0f77 37        scf     
0f78 c8        ret     z
0f79 3efc      ld      a,0fch
0f7b c9        ret     
0f7c af        xor     a
0f7d 3273a3    ld      (0a373h),a
0f80 210653    ld      hl,5306h
0f83 3a6fa3    ld      a,(0a36fh)
0f86 a7        and     a
0f87 280a      jr      z,0f93h
0f89 210c29    ld      hl,290ch
0f8c fe01      cp      01h
0f8e 2803      jr      z,0f93h
0f90 21001b    ld      hl,1b00h
0f93 2274a3    ld      (0a374h),hl
0f96 c9        ret     
0f97 cdcd0f    call    0fcdh
0f9a f5        push    af
0f9b 210d10    ld      hl,100dh
0f9e 1812      jr      0fb2h
0fa0 cdcd0f    call    0fcdh
0fa3 f5        push    af
0fa4 cdb910    call    10b9h
0fa7 214c10    ld      hl,104ch
0faa dcf20f    call    c,0ff2h
0fad dcce10    call    c,10ceh
0fb0 1808      jr      0fbah
0fb2 e5        push    hl
0fb3 cd6e10    call    106eh
0fb6 e1        pop     hl
0fb7 dcf20f    call    c,0ff2h
0fba d1        pop     de
0fbb f5        push    af
0fbc 0182f7    ld      bc,0f782h
0fbf ed49      out     (c),c
0fc1 0110f6    ld      bc,0f610h
0fc4 ed49      out     (c),c
0fc6 fb        ei      
0fc7 7a        ld      a,d
0fc8 cda611    call    11a6h
0fcb f1        pop     af
0fcc c9        ret     
0fcd 3270a3    ld      (0a370h),a
0fd0 1b        dec     de
0fd1 1c        inc     e
0fd2 e5        push    hl
0fd3 dde1      pop     ix
0fd5 cda011    call    11a0h
0fd8 f3        di      
0fd9 010ef4    ld      bc,0f40eh
0fdc ed49      out     (c),c
0fde 01d0f6    ld      bc,0f6d0h
0fe1 ed49      out     (c),c
0fe3 0e10      ld      c,10h
0fe5 ed49      out     (c),c
0fe7 0192f7    ld      bc,0f792h
0fea ed49      out     (c),c
0fec 0158f6    ld      bc,0f658h
0fef ed49      out     (c),c
0ff1 c9        ret     
0ff2 7a        ld      a,d
0ff3 b7        or      a
0ff4 280d      jr      z,1003h
0ff6 e5        push    hl
0ff7 d5        push    de
0ff8 1e00      ld      e,00h
0ffa cd0310    call    1003h
0ffd d1        pop     de
0ffe e1        pop     hl
0fff d0        ret     nc
1000 15        dec     d
1001 20f3      jr      nz,0ff6h
1003 01ffff    ld      bc,0ffffh
1006 ed4376a3  ld      (0a376h),bc
100a 1601      ld      d,01h
100c e9        jp      (hl)
100d cd0511    call    1105h
1010 d0        ret     nc
1011 dd7700    ld      (ix+00h),a
1014 dd23      inc     ix
1016 15        dec     d
1017 1d        dec     e
1018 20f3      jr      nz,100dh
101a 1812      jr      102eh
101c cd0511    call    1105h
101f d0        ret     nc
1020 47        ld      b,a
1021 dd7e00    ld      a,(ix+00h)
1024 a8        xor     b
1025 3e03      ld      a,03h
1027 c0        ret     nz
1028 dd23      inc     ix
102a 15        dec     d
102b 1d        dec     e
102c 20ee      jr      nz,101ch
102e 15        dec     d
102f 2806      jr      z,1037h
1031 cd0511    call    1105h
1034 d0        ret     nc
1035 18f7      jr      102eh
1037 cdfb10    call    10fbh
103a cd0511    call    1105h
103d d0        ret     nc
103e aa        xor     d
103f 2007      jr      nz,1048h
1041 cd0511    call    1105h
1044 d0        ret     nc
1045 ab        xor     e
1046 37        scf     
1047 c8        ret     z
1048 3e02      ld      a,02h
104a b7        or      a
104b c9        ret     
104c dd7e00    ld      a,(ix+00h)
104f cd4d11    call    114dh
1052 d0        ret     nc
1053 dd23      inc     ix
1055 15        dec     d
1056 1d        dec     e
1057 20f3      jr      nz,104ch
1059 15        dec     d
105a 2807      jr      z,1063h
105c af        xor     a
105d cd4d11    call    114dh
1060 d0        ret     nc
1061 18f6      jr      1059h
1063 cdfb10    call    10fbh
1066 cd4d11    call    114dh
1069 d0        ret     nc
106a 7b        ld      a,e
106b c34d11    jp      114dh
106e d5        push    de
106f cd7810    call    1078h
1072 d1        pop     de
1073 d8        ret     c
1074 b7        or      a
1075 c8        ret     z
1076 18f6      jr      106eh
1078 2e55      ld      l,55h
107a cd2211    call    1122h
107d d0        ret     nc
107e 110000    ld      de,0000h
1081 62        ld      h,d
1082 cd2211    call    1122h
1085 d0        ret     nc
1086 eb        ex      de,hl
1087 0600      ld      b,00h
1089 09        add     hl,bc
108a eb        ex      de,hl
108b 25        dec     h
108c 20f4      jr      nz,1082h
108e 61        ld      h,c
108f 79        ld      a,c
1090 92        sub     d
1091 4f        ld      c,a
1092 9f        sbc     a,a
1093 47        ld      b,a
1094 eb        ex      de,hl
1095 09        add     hl,bc
1096 eb        ex      de,hl
1097 cd2211    call    1122h
109a d0        ret     nc
109b 7a        ld      a,d
109c cb3f      srl     a
109e cb3f      srl     a
10a0 8a        adc     a,d
10a1 94        sub     h
10a2 38ea      jr      c,108eh
10a4 91        sub     c
10a5 38e7      jr      c,108eh
10a7 7a        ld      a,d
10a8 1f        rra     
10a9 8a        adc     a,d
10aa 67        ld      h,a
10ab 2271a3    ld      (0a371h),hl
10ae cd0511    call    1105h
10b1 d0        ret     nc
10b2 2170a3    ld      hl,0a370h
10b5 ae        xor     (hl)
10b6 c0        ret     nz
10b7 37        scf     
10b8 c9        ret     
10b9 cddc11    call    11dch
10bc 210108    ld      hl,0801h
10bf cdd110    call    10d1h
10c2 d0        ret     nc
10c3 b7        or      a
10c4 cd5d11    call    115dh
10c7 d0        ret     nc
10c8 3a70a3    ld      a,(0a370h)
10cb c34d11    jp      114dh
10ce 212100    ld      hl,0021h
10d1 06f4      ld      b,0f4h
10d3 ed78      in      a,(c)
10d5 e604      and     04h
10d7 c8        ret     z
10d8 e5        push    hl
10d9 37        scf     
10da cd5d11    call    115dh
10dd e1        pop     hl
10de 2b        dec     hl
10df 7c        ld      a,h
10e0 b5        or      l
10e1 20ee      jr      nz,10d1h
10e3 37        scf     
10e4 c9        ret     
10e5 2a76a3    ld      hl,(0a376h)
10e8 ac        xor     h
10e9 f2f510    jp      p,10f5h
10ec 7c        ld      a,h
10ed ee08      xor     08h
10ef 67        ld      h,a
10f0 7d        ld      a,l
10f1 ee10      xor     10h
10f3 6f        ld      l,a
10f4 37        scf     
10f5 ed6a      adc     hl,hl
10f7 2276a3    ld      (0a376h),hl
10fa c9        ret     
10fb 2a76a3    ld      hl,(0a376h)
10fe 7d        ld      a,l
10ff 2f        cpl     
1100 5f        ld      e,a
1101 7c        ld      a,h
1102 2f        cpl     
1103 57        ld      d,a
1104 c9        ret     
1105 d5        push    de
1106 1e08      ld      e,08h
1108 2a71a3    ld      hl,(0a371h)
110b cd2911    call    1129h
110e dc3211    call    c,1132h
1111 300d      jr      nc,1120h
1113 7c        ld      a,h
1114 91        sub     c
1115 9f        sbc     a,a
1116 cb12      rl      d
1118 cde510    call    10e5h
111b 1d        dec     e
111c 20ea      jr      nz,1108h
111e 7a        ld      a,d
111f 37        scf     
1120 d1        pop     de
1121 c9        ret     
1122 06f4      ld      b,0f4h
1124 ed78      in      a,(c)
1126 e604      and     04h
1128 c8        ret     z
1129 ed5f      ld      a,r
112b c603      add     a,03h
112d 0f        rrca    
112e 0f        rrca    
112f e61f      and     1fh
1131 4f        ld      c,a
1132 06f5      ld      b,0f5h
1134 79        ld      a,c
1135 c602      add     a,02h
1137 4f        ld      c,a
1138 380e      jr      c,1148h
113a ed78      in      a,(c)
113c ad        xor     l
113d e680      and     80h
113f 20f3      jr      nz,1134h
1141 af        xor     a
1142 ed4f      ld      r,a
1144 cb0d      rrc     l
1146 37        scf     
1147 c9        ret     
1148 af        xor     a
1149 ed4f      ld      r,a
114b 3c        inc     a
114c c9        ret     
114d d5        push    de
114e 1e08      ld      e,08h
1150 57        ld      d,a
1151 cb02      rlc     d
1153 cd5d11    call    115dh
1156 3003      jr      nc,115bh
1158 1d        dec     e
1159 20f6      jr      nz,1151h
115b d1        pop     de
115c c9        ret     
115d ed4b73a3  ld      bc,(0a373h)
1161 2a75a3    ld      hl,(0a375h)
1164 9f        sbc     a,a
1165 67        ld      h,a
1166 2807      jr      z,116fh
1168 7d        ld      a,l
1169 87        add     a,a
116a 80        add     a,b
116b 6f        ld      l,a
116c 79        ld      a,c
116d 90        sub     b
116e 4f        ld      c,a
116f 7d        ld      a,l
1170 3273a3    ld      (0a373h),a
1173 2e0a      ld      l,0ah
1175 cd8c11    call    118ch
1178 3806      jr      c,1180h
117a 91        sub     c
117b 300c      jr      nc,1189h
117d 2f        cpl     
117e 3c        inc     a
117f 4f        ld      c,a
1180 7c        ld      a,h
1181 cde510    call    10e5h
1184 2e0b      ld      l,0bh
1186 cd8c11    call    118ch
1189 3e01      ld      a,01h
118b c9        ret     
118c ed5f      ld      a,r
118e cb3f      srl     a
1190 91        sub     c
1191 3003      jr      nc,1196h
1193 3c        inc     a
1194 20fd      jr      nz,1193h
1196 06f7      ld      b,0f7h
1198 ed69      out     (c),l
119a f5        push    af
119b af        xor     a
119c ed4f      ld      r,a
119e f1        pop     af
119f c9        ret     
11a0 3e10      ld      a,10h
11a2 1802      jr      11a6h
11a4 3eef      ld      a,0efh
11a6 c5        push    bc
11a7 06f6      ld      b,0f6h
11a9 ed48      in      c,(c)
11ab 04        inc     b
11ac e610      and     10h
11ae 3e08      ld      a,08h
11b0 2801      jr      z,11b3h
11b2 3c        inc     a
11b3 ed79      out     (c),a
11b5 37        scf     
11b6 280c      jr      z,11c4h
11b8 79        ld      a,c
11b9 e610      and     10h
11bb c5        push    bc
11bc 01c800    ld      bc,00c8h
11bf 37        scf     
11c0 ccc711    call    z,11c7h
11c3 c1        pop     bc
11c4 79        ld      a,c
11c5 c1        pop     bc
11c6 c9        ret     
11c7 c5        push    bc
11c8 e5        push    hl
11c9 cddc11    call    11dch
11cc cdf013    call    13f0h
11cf e1        pop     hl
11d0 c1        pop     bc
11d1 2007      jr      nz,11dah
11d3 0b        dec     bc
11d4 78        ld      a,b
11d5 b1        or      c
11d6 20ef      jr      nz,11c7h
11d8 37        scf     
11d9 c9        ret     
11da af        xor     a
11db c9        ret     
11dc 018206    ld      bc,0682h
11df 0b        dec     bc
11e0 78        ld      a,b
11e1 b1        or      c
11e2 20fb      jr      nz,11dfh
11e4 c9        ret     
11e5 ef        rst     28h
11e6 f1        pop     af
11e7 1d        dec     e
11e8 3e01      ld      a,01h
11ea 210e00    ld      hl,000eh
11ed cdc016    call    16c0h
11f0 d8        ret     c
11f1 3a0ba3    ld      a,(0a30bh)
11f4 fe43      cp      43h
11f6 30f0      jr      nc,11e8h
11f8 fe41      cp      41h
11fa 38ec      jr      c,11e8h
11fc f5        push    af
11fd cd541c    call    1c54h
1200 cd4a15    call    154ah
1203 cdae1a    call    1aaeh
1206 cd5815    call    1558h
1209 f1        pop     af
120a cdea0e    call    0eeah
120d af        xor     a
120e fd2a01a3  ld      iy,(0a301h)
1212 cd2200    call    0022h
1215 3e0d      ld      a,0dh
1217 cd6e15    call    156eh
121a cd6e15    call    156eh
121d cd6e15    call    156eh
1220 cd6e15    call    156eh
1223 3eff      ld      a,0ffh
1225 3268a3    ld      (0a368h),a
1228 3269a3    ld      (0a369h),a
122b fd2a01a3  ld      iy,(0a301h)
122f 1100ac    ld      de,0ac00h
1232 cd9bbc    call    0bc9bh
1235 3805      jr      c,123ch
1237 2003      jr      nz,123ch
1239 cdfc08    call    08fch
123c af        xor     a
123d 3268a3    ld      (0a368h),a
1240 c9        ret     
1241 cd5b00    call    005bh
1244 283a      jr      z,1280h
1246 111800    ld      de,0018h
1249 19        add     hl,de
124a 013f00    ld      bc,003fh
124d 71        ld      (hl),c
124e 23        inc     hl
124f 70        ld      (hl),b
1250 cd5600    call    0056h
1253 280b      jr      z,1260h
1255 11faff    ld      de,0fffah
1258 19        add     hl,de
1259 71        ld      (hl),c
125a 23        inc     hl
125b 70        ld      (hl),b
125c cd670f    call    0f67h
125f d8        ret     c
1260 21eebf    ld      hl,0bfeeh
1263 1100a2    ld      de,0a200h
1266 011000    ld      bc,0010h
1269 edb0      ldir    
126b 3a6da3    ld      a,(0a36dh)
126e 3210a2    ld      (0a210h),a
1271 3eff      ld      a,0ffh
1273 323da2    ld      (0a23dh),a
1276 2100a2    ld      hl,0a200h
1279 013f00    ld      bc,003fh
127c cd8708    call    0887h
127f c9        ret     
1280 3e28      ld      a,28h
1282 3227a2    ld      (0a227h),a
1285 3e19      ld      a,19h
1287 3228a2    ld      (0a228h),a
128a 3e12      ld      a,12h
128c 323ea2    ld      (0a23eh),a
128f af        xor     a
1290 323ca2    ld      (0a23ch),a
1293 323da2    ld      (0a23dh),a
1296 e5        push    hl
1297 d5        push    de
1298 c5        push    bc
1299 21b103    ld      hl,03b1h
129c 1129a2    ld      de,0a229h
129f 011100    ld      bc,0011h
12a2 edb0      ldir    
12a4 2100c0    ld      hl,0c000h
12a7 223aa2    ld      (0a23ah),hl
12aa c1        pop     bc
12ab d1        pop     de
12ac e1        pop     hl
12ad 111800    ld      de,0018h
12b0 19        add     hl,de
12b1 4e        ld      c,(hl)
12b2 23        inc     hl
12b3 46        ld      b,(hl)
12b4 2100a2    ld      hl,0a200h
12b7 cd8708    call    0887h
12ba d8        ret     c
12bb 2100a2    ld      hl,0a200h
12be 11eebf    ld      de,0bfeeh
12c1 011000    ld      bc,0010h
12c4 edb0      ldir    
12c6 7e        ld      a,(hl)
12c7 a7        and     a
12c8 c8        ret     z
12c9 3a6da3    ld      a,(0a36dh)
12cc be        cp      (hl)
12cd 280f      jr      z,12deh
12cf cd5600    call    0056h
12d2 2006      jr      nz,12dah
12d4 cd6000    call    0060h
12d7 c47dbc    call    nz,0bc7dh
12da 3efd      ld      a,0fdh
12dc 37        scf     
12dd c9        ret     
12de a7        and     a
12df c9        ret     
12e0 e5        push    hl
12e1 111200    ld      de,0012h
12e4 19        add     hl,de
12e5 cd5b00    call    005bh
12e8 281a      jr      z,1304h
12ea 3602      ld      (hl),02h
12ec 110800    ld      de,0008h
12ef 19        add     hl,de
12f0 ed5b0200  ld      de,(0002h)
12f4 73        ld      (hl),e
12f5 23        inc     hl
12f6 72        ld      (hl),d
12f7 23        inc     hl
12f8 eb        ex      de,hl
12f9 21a81d    ld      hl,1da8h
12fc 012400    ld      bc,0024h
12ff edb0      ldir    
1301 a7        and     a
1302 e1        pop     hl
1303 c9        ret     
1304 7e        ld      a,(hl)
1305 fe02      cp      02h
1307 2804      jr      z,130dh
1309 fe03      cp      03h
130b 2034      jr      nz,1341h
130d eb        ex      de,hl
130e 210800    ld      hl,0008h
1311 19        add     hl,de
1312 3a0200    ld      a,(0002h)
1315 be        cp      (hl)
1316 2029      jr      nz,1341h
1318 23        inc     hl
1319 3a0300    ld      a,(0003h)
131c be        cp      (hl)
131d 2022      jr      nz,1341h
131f eb        ex      de,hl
1320 11a81d    ld      de,1da8h
1323 cd2a13    call    132ah
1326 30d9      jr      nc,1301h
1328 1817      jr      1341h
132a e5        push    hl
132b d5        push    de
132c 110a00    ld      de,000ah
132f 19        add     hl,de
1330 0624      ld      b,24h
1332 d1        pop     de
1333 1a        ld      a,(de)
1334 be        cp      (hl)
1335 2803      jr      z,133ah
1337 e1        pop     hl
1338 37        scf     
1339 c9        ret     
133a 23        inc     hl
133b 13        inc     de
133c 10f5      djnz    1333h
133e e1        pop     hl
133f a7        and     a
1340 c9        ret     
1341 3efe      ld      a,0feh
1343 e1        pop     hl
1344 37        scf     
1345 c9        ret     
1346 06f4      ld      b,0f4h
1348 ed49      out     (c),c
134a 01cff6    ld      bc,0f6cfh
134d ed49      out     (c),c
134f 0e0f      ld      c,0fh
1351 ed49      out     (c),c
1353 0100f4    ld      bc,0f400h
1356 ed79      out     (c),a
1358 018ff6    ld      bc,0f68fh
135b ed49      out     (c),c
135d 0e0f      ld      c,0fh
135f ed49      out     (c),c
1361 c9        ret     
1362 3eff      ld      a,0ffh
1364 0e0f      ld      c,0fh
1366 06fe      ld      b,0feh
1368 ed49      out     (c),c
136a 51        ld      d,c
136b 0100fd    ld      bc,0fd00h
136e ed79      out     (c),a
1370 4a        ld      c,d
1371 0d        dec     c
1372 f26613    jp      p,1366h
1375 c9        ret     
1376 0100fe    ld      bc,0fe00h
1379 ed49      out     (c),c
137b 0140fc    ld      bc,0fc40h
137e 213b00    ld      hl,003bh
1381 1603      ld      d,03h
1383 ed78      in      a,(c)
1385 be        cp      (hl)
1386 c0        ret     nz
1387 23        inc     hl
1388 03        inc     bc
1389 15        dec     d
138a 20f7      jr      nz,1383h
138c 3eff      ld      a,0ffh
138e 1616      ld      d,16h
1390 ed58      in      e,(c)
1392 ab        xor     e
1393 03        inc     bc
1394 15        dec     d
1395 20f9      jr      nz,1390h
1397 ed58      in      e,(c)
1399 bb        cp      e
139a c9        ret     
139b 0e00      ld      c,00h
139d 06df      ld      b,0dfh
139f ed49      out     (c),c
13a1 1111a2    ld      de,0a211h
13a4 2100c0    ld      hl,0c000h
13a7 0606      ld      b,06h
13a9 1a        ld      a,(de)
13aa be        cp      (hl)
13ab 2018      jr      nz,13c5h
13ad 10fa      djnz    13a9h
13af ed5b15a2  ld      de,(0a215h)
13b3 7a        ld      a,d
13b4 fec0      cp      0c0h
13b6 380b      jr      c,13c3h
13b8 0610      ld      b,10h
13ba 2117a2    ld      hl,0a217h
13bd 1a        ld      a,(de)
13be be        cp      (hl)
13bf 2004      jr      nz,13c5h
13c1 10fa      djnz    13bdh
13c3 a7        and     a
13c4 c9        ret     
13c5 0c        inc     c
13c6 20d5      jr      nz,139dh
13c8 ef        rst     28h
13c9 c8        ret     z
13ca 1f        rra     
13cb f7        rst     30h
13cc 0e1f      ld      c,1fh
13ce cd1114    call    1411h
13d1 0107df    ld      bc,0df07h
13d4 ed49      out     (c),c
13d6 37        scf     
13d7 c9        ret     
13d8 010600    ld      bc,0006h
13db 2100c0    ld      hl,0c000h
13de 1111a2    ld      de,0a211h
13e1 edb0      ldir    
13e3 2a04c0    ld      hl,(0c004h)
13e6 7c        ld      a,h
13e7 fec0      cp      0c0h
13e9 d8        ret     c
13ea 011000    ld      bc,0010h
13ed edb0      ldir    
13ef c9        ret     
13f0 af        xor     a
13f1 323aa3    ld      (0a33ah),a
13f4 3e02      ld      a,02h
13f6 3239a3    ld      (0a339h),a
13f9 cd8714    call    1487h
13fc 3a3aa3    ld      a,(0a33ah)
13ff a7        and     a
1400 2001      jr      nz,1403h
1402 c9        ret     
1403 3a3ba3    ld      a,(0a33bh)
1406 fe1b      cp      1bh
1408 2802      jr      z,140ch
140a af        xor     a
140b c9        ret     
140c 3eff      ld      a,0ffh
140e a7        and     a
140f 37        scf     
1410 c9        ret     
1411 af        xor     a
1412 323aa3    ld      (0a33ah),a
1415 326ba3    ld      (0a36bh),a
1418 3e0e      ld      a,0eh
141a 326ca3    ld      (0a36ch),a
141d 01c409    ld      bc,09c4h
1420 0b        dec     bc
1421 78        ld      a,b
1422 b1        or      c
1423 20fb      jr      nz,1420h
1425 3a6aa3    ld      a,(0a36ah)
1428 a7        and     a
1429 284b      jr      z,1476h
142b 3a6ca3    ld      a,(0a36ch)
142e 3c        inc     a
142f 326ca3    ld      (0a36ch),a
1432 fe0f      cp      0fh
1434 2040      jr      nz,1476h
1436 af        xor     a
1437 326ca3    ld      (0a36ch),a
143a 3a07a3    ld      a,(0a307h)
143d 47        ld      b,a
143e 3a09a3    ld      a,(0a309h)
1441 80        add     a,b
1442 3203a3    ld      (0a303h),a
1445 3a6ba3    ld      a,(0a36bh)
1448 cd6e15    call    156eh
144b ee01      xor     01h
144d 326ba3    ld      (0a36bh),a
1450 210ba3    ld      hl,0a30bh
1453 1600      ld      d,00h
1455 3a09a3    ld      a,(0a309h)
1458 5f        ld      e,a
1459 19        add     hl,de
145a 7e        ld      a,(hl)
145b cd6e15    call    156eh
145e 3e00      ld      a,00h
1460 cd6e15    call    156eh
1463 3a7aa3    ld      a,(0a37ah)
1466 a7        and     a
1467 280d      jr      z,1476h
1469 2a78a3    ld      hl,(0a378h)
146c 47        ld      b,a
146d 3a6ba3    ld      a,(0a36bh)
1470 a7        and     a
1471 2802      jr      z,1475h
1473 0654      ld      b,54h
1475 70        ld      (hl),b
1476 cd8714    call    1487h
1479 3a3aa3    ld      a,(0a33ah)
147c a7        and     a
147d 289e      jr      z,141dh
147f af        xor     a
1480 326aa3    ld      (0a36ah),a
1483 3a3ba3    ld      a,(0a33bh)
1486 c9        ret     
1487 0182f7    ld      bc,0f782h
148a ed49      out     (c),c
148c 010ef4    ld      bc,0f40eh
148f ed49      out     (c),c
1491 01c0f6    ld      bc,0f6c0h
1494 ed49      out     (c),c
1496 0e00      ld      c,00h
1498 ed49      out     (c),c
149a 0192f7    ld      bc,0f792h
149d ed49      out     (c),c
149f 260a      ld      h,0ah
14a1 7c        ld      a,h
14a2 3d        dec     a
14a3 f640      or      40h
14a5 01fff6    ld      bc,0f6ffh
14a8 ed79      out     (c),a
14aa 01fff4    ld      bc,0f4ffh
14ad ed78      in      a,(c)
14af feff      cp      0ffh
14b1 200c      jr      nz,14bfh
14b3 25        dec     h
14b4 20eb      jr      nz,14a1h
14b6 af        xor     a
14b7 3239a3    ld      (0a339h),a
14ba 323aa3    ld      (0a33ah),a
14bd 1835      jr      14f4h
14bf 0608      ld      b,08h
14c1 17        rla     
14c2 3004      jr      nc,14c8h
14c4 10fb      djnz    14c1h
14c6 18d9      jr      14a1h
14c8 05        dec     b
14c9 25        dec     h
14ca 7c        ld      a,h
14cb 07        rlca    
14cc 07        rlca    
14cd 07        rlca    
14ce b0        or      b
14cf 0600      ld      b,00h
14d1 4f        ld      c,a
14d2 3a3aa3    ld      a,(0a33ah)
14d5 a7        and     a
14d6 201c      jr      nz,14f4h
14d8 3a39a3    ld      a,(0a339h)
14db 3c        inc     a
14dc 3239a3    ld      (0a339h),a
14df fe03      cp      03h
14e1 2011      jr      nz,14f4h
14e3 21fa14    ld      hl,14fah
14e6 09        add     hl,bc
14e7 7e        ld      a,(hl)
14e8 feff      cp      0ffh
14ea 2808      jr      z,14f4h
14ec 323ba3    ld      (0a33bh),a
14ef 3eff      ld      a,0ffh
14f1 323aa3    ld      (0a33ah),a
14f4 0182f7    ld      bc,0f782h
14f7 ed49      out     (c),c
14f9 c9        ret     
14fa ff        rst     38h
14fb f3        di      
14fc ff        rst     38h
14fd 39        add     hl,sp
14fe 3633      ld      (hl),33h
1500 0d        dec     c
1501 ff        rst     38h
1502 f2ff37    jp      p,37ffh
1505 3835      jr      c,153ch
1507 313230    ld      sp,3032h
150a 10ff      djnz    150bh
150c 0d        dec     c
150d ff        rst     38h
150e 34        inc     (hl)
150f ff        rst     38h
1510 ff        rst     38h
1511 ff        rst     38h
1512 ff        rst     38h
1513 ff        rst     38h
1514 ff        rst     38h
1515 50        ld      d,b
1516 ff        rst     38h
1517 ff        rst     38h
1518 ff        rst     38h
1519 ff        rst     38h
151a 3039      jr      nc,1555h
151c 4f        ld      c,a
151d 49        ld      c,c
151e 4c        ld      c,h
151f 4b        ld      c,e
1520 4d        ld      c,l
1521 ff        rst     38h
1522 3837      jr      c,155bh
1524 55        ld      d,l
1525 59        ld      e,c
1526 48        ld      c,b
1527 4a        ld      c,d
1528 4e        ld      c,(hl)
1529 ff        rst     38h
152a 3635      ld      (hl),35h
152c 52        ld      d,d
152d 54        ld      d,h
152e 47        ld      b,a
152f 46        ld      b,(hl)
1530 42        ld      b,d
1531 56        ld      d,(hl)
1532 34        inc     (hl)
1533 33        inc     sp
1534 45        ld      b,l
1535 57        ld      d,a
1536 53        ld      d,e
1537 44        ld      b,h
1538 43        ld      b,e
1539 58        ld      e,b
153a 31321b    ld      sp,1b32h
153d 51        ld      d,c
153e ff        rst     38h
153f 41        ld      b,c
1540 ff        rst     38h
1541 5a        ld      e,d
1542 ff        rst     38h
1543 ff        rst     38h
1544 ff        rst     38h
1545 ff        rst     38h
1546 ff        rst     38h
1547 ff        rst     38h
1548 ff        rst     38h
1549 7f        ld      a,a
154a 01107f    ld      bc,7f10h
154d 3e44      ld      a,44h
154f ed49      out     (c),c
1551 ed79      out     (c),a
1553 0d        dec     c
1554 f24f15    jp      p,154fh
1557 c9        ret     
1558 21b103    ld      hl,03b1h
155b 1803      jr      1560h
155d 2129a2    ld      hl,0a229h
1560 01107f    ld      bc,7f10h
1563 ed49      out     (c),c
1565 7e        ld      a,(hl)
1566 23        inc     hl
1567 ed79      out     (c),a
1569 0d        dec     c
156a f26315    jp      p,1563h
156d c9        ret     
156e e5        push    hl
156f d5        push    de
1570 c5        push    bc
1571 f5        push    af
1572 fe02      cp      02h
1574 3006      jr      nc,157ch
1576 3205a3    ld      (0a305h),a
1579 c32216    jp      1622h
157c 200f      jr      nz,158dh
157e 3a03a3    ld      a,(0a303h)
1581 3d        dec     a
1582 feff      cp      0ffh
1584 ca2216    jp      z,1622h
1587 3203a3    ld      (0a303h),a
158a c32216    jp      1622h
158d fe0d      cp      0dh
158f 2004      jr      nz,1595h
1591 3e27      ld      a,27h
1593 1868      jr      15fdh
1595 fe1d      cp      1dh
1597 da2216    jp      c,1622h
159a f5        push    af
159b 2600      ld      h,00h
159d 6f        ld      l,a
159e 3e08      ld      a,08h
15a0 cd2716    call    1627h
15a3 f1        pop     af
15a4 fe20      cp      20h
15a6 3006      jr      nc,15aeh
15a8 11fb1e    ld      de,1efbh
15ab 19        add     hl,de
15ac 180a      jr      15b8h
15ae 110038    ld      de,3800h
15b1 19        add     hl,de
15b2 cd3c16    call    163ch
15b5 2192a3    ld      hl,0a392h
15b8 e5        push    hl
15b9 2a03a3    ld      hl,(0a303h)
15bc 2600      ld      h,00h
15be 3e02      ld      a,02h
15c0 cd2716    call    1627h
15c3 eb        ex      de,hl
15c4 2a04a3    ld      hl,(0a304h)
15c7 2600      ld      h,00h
15c9 3e50      ld      a,50h
15cb cd2716    call    1627h
15ce 19        add     hl,de
15cf 1100c0    ld      de,0c000h
15d2 19        add     hl,de
15d3 eb        ex      de,hl
15d4 e1        pop     hl
15d5 0608      ld      b,08h
15d7 7e        ld      a,(hl)
15d8 23        inc     hl
15d9 e5        push    hl
15da 67        ld      h,a
15db 3a05a3    ld      a,(0a305h)
15de fe01      cp      01h
15e0 7c        ld      a,h
15e1 2001      jr      nz,15e4h
15e3 2f        cpl     
15e4 67        ld      h,a
15e5 e6f0      and     0f0h
15e7 12        ld      (de),a
15e8 13        inc     de
15e9 7c        ld      a,h
15ea e60f      and     0fh
15ec 0f        rrca    
15ed 0f        rrca    
15ee 0f        rrca    
15ef 0f        rrca    
15f0 12        ld      (de),a
15f1 eb        ex      de,hl
15f2 11ff07    ld      de,07ffh
15f5 19        add     hl,de
15f6 eb        ex      de,hl
15f7 e1        pop     hl
15f8 10dd      djnz    15d7h
15fa 3a03a3    ld      a,(0a303h)
15fd 3c        inc     a
15fe 3203a3    ld      (0a303h),a
1601 fe28      cp      28h
1603 381d      jr      c,1622h
1605 af        xor     a
1606 3203a3    ld      (0a303h),a
1609 3a04a3    ld      a,(0a304h)
160c 3c        inc     a
160d 3204a3    ld      (0a304h),a
1610 fe17      cp      17h
1612 200e      jr      nz,1622h
1614 f7        rst     30h
1615 0e1f      ld      c,1fh
1617 cd1114    call    1411h
161a cdae1a    call    1aaeh
161d 3e05      ld      a,05h
161f 3204a3    ld      (0a304h),a
1622 f1        pop     af
1623 c1        pop     bc
1624 d1        pop     de
1625 e1        pop     hl
1626 c9        ret     
1627 d5        push    de
1628 54        ld      d,h
1629 5d        ld      e,l
162a 210000    ld      hl,0000h
162d cb3f      srl     a
162f 3001      jr      nc,1632h
1631 19        add     hl,de
1632 2806      jr      z,163ah
1634 cb23      sla     e
1636 cb12      rl      d
1638 18f3      jr      162dh
163a d1        pop     de
163b c9        ret     
163c e5        push    hl
163d 215016    ld      hl,1650h
1640 1100a1    ld      de,0a100h
1643 012f00    ld      bc,002fh
1646 edb0      ldir    
1648 e1        pop     hl
1649 1192a3    ld      de,0a392h
164c cd00a1    call    0a100h
164f c9        ret     
1650 3a9ca3    ld      a,(0a39ch)
1653 fe01      cp      01h
1655 28f9      jr      z,1650h
1657 f3        di      
1658 cdfb3f    call    3ffbh
165b 067f      ld      b,7fh
165d 3a9da3    ld      a,(0a39dh)
1660 e683      and     83h
1662 ed79      out     (c),a
1664 010800    ld      bc,0008h
1667 edb0      ldir    
1669 067f      ld      b,7fh
166b f604      or      04h
166d ed79      out     (c),a
166f 3afb3f    ld      a,(3ffbh)
1672 21fb3f    ld      hl,3ffbh
1675 36c9      ld      (hl),0c9h
1677 cdfb3f    call    3ffbh
167a 32fb3f    ld      (3ffbh),a
167d fb        ei      
167e c9        ret     
167f 3a06a3    ld      a,(0a306h)
1682 3204a3    ld      (0a304h),a
1685 3a07a3    ld      a,(0a307h)
1688 3203a3    ld      (0a303h),a
168b 210ba3    ld      hl,0a30bh
168e 3a08a3    ld      a,(0a308h)
1691 47        ld      b,a
1692 7e        ld      a,(hl)
1693 c5        push    bc
1694 e5        push    hl
1695 cd6e15    call    156eh
1698 e1        pop     hl
1699 c1        pop     bc
169a 23        inc     hl
169b 10f5      djnz    1692h
169d 3e20      ld      a,20h
169f cd6e15    call    156eh
16a2 c9        ret     
16a3 3208a3    ld      (0a308h),a
16a6 47        ld      b,a
16a7 7c        ld      a,h
16a8 3206a3    ld      (0a306h),a
16ab 7d        ld      a,l
16ac 3207a3    ld      (0a307h),a
16af af        xor     a
16b0 3209a3    ld      (0a309h),a
16b3 320aa3    ld      (0a30ah),a
16b6 210ba3    ld      hl,0a30bh
16b9 04        inc     b
16ba 3620      ld      (hl),20h
16bc 23        inc     hl
16bd 10fb      djnz    16bah
16bf c9        ret     
16c0 cda316    call    16a3h
16c3 cd7f16    call    167fh
16c6 3eff      ld      a,0ffh
16c8 326aa3    ld      (0a36ah),a
16cb cd1114    call    1411h
16ce f5        push    af
16cf af        xor     a
16d0 326aa3    ld      (0a36ah),a
16d3 f1        pop     af
16d4 fe1b      cp      1bh
16d6 37        scf     
16d7 c8        ret     z
16d8 fe0d      cp      0dh
16da 200e      jr      nz,16eah
16dc 3a0aa3    ld      a,(0a30ah)
16df a7        and     a
16e0 28e4      jr      z,16c6h
16e2 3209a3    ld      (0a309h),a
16e5 cd7f16    call    167fh
16e8 a7        and     a
16e9 c9        ret     
16ea fef2      cp      0f2h
16ec 200c      jr      nz,16fah
16ee 3a09a3    ld      a,(0a309h)
16f1 a7        and     a
16f2 28d2      jr      z,16c6h
16f4 3d        dec     a
16f5 3209a3    ld      (0a309h),a
16f8 18c9      jr      16c3h
16fa fef3      cp      0f3h
16fc 2010      jr      nz,170eh
16fe 3a0aa3    ld      a,(0a30ah)
1701 47        ld      b,a
1702 3a09a3    ld      a,(0a309h)
1705 b8        cp      b
1706 30be      jr      nc,16c6h
1708 3c        inc     a
1709 3209a3    ld      (0a309h),a
170c 18b5      jr      16c3h
170e fe7f      cp      7fh
1710 2034      jr      nz,1746h
1712 3a09a3    ld      a,(0a309h)
1715 a7        and     a
1716 28ae      jr      z,16c6h
1718 210ba3    ld      hl,0a30bh
171b ed5b08a3  ld      de,(0a308h)
171f 1600      ld      d,00h
1721 19        add     hl,de
1722 3620      ld      (hl),20h
1724 3d        dec     a
1725 3209a3    ld      (0a309h),a
1728 5f        ld      e,a
1729 3a0aa3    ld      a,(0a30ah)
172c 3d        dec     a
172d 320aa3    ld      (0a30ah),a
1730 3c        inc     a
1731 93        sub     e
1732 47        ld      b,a
1733 dd210ba3  ld      ix,0a30bh
1737 dd19      add     ix,de
1739 dd7e01    ld      a,(ix+01h)
173c dd7700    ld      (ix+00h),a
173f dd23      inc     ix
1741 10f6      djnz    1739h
1743 c3c316    jp      16c3h
1746 fe10      cp      10h
1748 200e      jr      nz,1758h
174a 3a0aa3    ld      a,(0a30ah)
174d 5f        ld      e,a
174e 3a09a3    ld      a,(0a309h)
1751 bb        cp      e
1752 cac616    jp      z,16c6h
1755 3c        inc     a
1756 18c0      jr      1718h
1758 fe80      cp      80h
175a d2c616    jp      nc,16c6h
175d fe20      cp      20h
175f dac616    jp      c,16c6h
1762 f5        push    af
1763 3a09a3    ld      a,(0a309h)
1766 5f        ld      e,a
1767 3a08a3    ld      a,(0a308h)
176a bb        cp      e
176b 2004      jr      nz,1771h
176d f1        pop     af
176e c3c616    jp      16c6h
1771 f1        pop     af
1772 210ba3    ld      hl,0a30bh
1775 ed5b09a3  ld      de,(0a309h)
1779 1600      ld      d,00h
177b 19        add     hl,de
177c 77        ld      (hl),a
177d 7b        ld      a,e
177e 3c        inc     a
177f 3209a3    ld      (0a309h),a
1782 5f        ld      e,a
1783 3a0aa3    ld      a,(0a30ah)
1786 bb        cp      e
1787 d2c316    jp      nc,16c3h
178a 7b        ld      a,e
178b 320aa3    ld      (0a30ah),a
178e c3c316    jp      16c3h
1791 cda316    call    16a3h
1794 62        ld      h,d
1795 6b        ld      l,e
1796 cd4d1b    call    1b4dh
1799 c3c316    jp      16c3h
179c ef        rst     28h
179d 3d        dec     a
179e 1e3e      ld      e,3eh
17a0 02        ld      (bc),a
17a1 ed5b27a2  ld      de,(0a227h)
17a5 1600      ld      d,00h
17a7 211500    ld      hl,0015h
17aa cd9117    call    1791h
17ad d8        ret     c
17ae cd1b19    call    191bh
17b1 38ec      jr      c,179fh
17b3 7b        ld      a,e
17b4 fe0a      cp      0ah
17b6 38e7      jr      c,179fh
17b8 fe2f      cp      2fh
17ba 30e3      jr      nc,179fh
17bc 3227a2    ld      (0a227h),a
17bf f7        rst     30h
17c0 52        ld      d,d
17c1 1e3e      ld      e,3eh
17c3 02        ld      (bc),a
17c4 ed5b28a2  ld      de,(0a228h)
17c8 1600      ld      d,00h
17ca 211601    ld      hl,0116h
17cd cd9117    call    1791h
17d0 d8        ret     c
17d1 cd1b19    call    191bh
17d4 38ec      jr      c,17c2h
17d6 7b        ld      a,e
17d7 fe0a      cp      0ah
17d9 38e7      jr      c,17c2h
17db fe21      cp      21h
17dd 30e3      jr      nc,17c2h
17df 3228a2    ld      (0a228h),a
17e2 ef        rst     28h
17e3 69        ld      l,c
17e4 1e3e      ld      e,3eh
17e6 02        ld      (bc),a
17e7 ed5b3ea2  ld      de,(0a23eh)
17eb 1600      ld      d,00h
17ed 211900    ld      hl,0019h
17f0 cd9117    call    1791h
17f3 d8        ret     c
17f4 cd1b19    call    191bh
17f7 38ec      jr      c,17e5h
17f9 7b        ld      a,e
17fa fe0a      cp      0ah
17fc 38e7      jr      c,17e5h
17fe fe17      cp      17h
1800 30e3      jr      nc,17e5h
1802 323ea2    ld      (0a23eh),a
1805 a7        and     a
1806 c9        ret     
1807 ef        rst     28h
1808 99        sbc     a,c
1809 1e3e      ld      e,3eh
180b 02        ld      (bc),a
180c 211b00    ld      hl,001bh
180f cdc016    call    16c0h
1812 d8        ret     c
1813 cd1b19    call    191bh
1816 38f2      jr      c,180ah
1818 7b        ld      a,e
1819 fe11      cp      11h
181b 30ed      jr      nc,180ah
181d 1600      ld      d,00h
181f 2139a2    ld      hl,0a239h
1822 a7        and     a
1823 ed52      sbc     hl,de
1825 2278a3    ld      (0a378h),hl
1828 f7        rst     30h
1829 b4        or      h
182a 1e2a      ld      e,2ah
182c 78        ld      a,b
182d a3        and     e
182e 7e        ld      a,(hl)
182f f5        push    af
1830 fe54      cp      54h
1832 2002      jr      nz,1836h
1834 3e40      ld      a,40h
1836 327aa3    ld      (0a37ah),a
1839 7e        ld      a,(hl)
183a e61f      and     1fh
183c 57        ld      d,a
183d 21a218    ld      hl,18a2h
1840 1e1a      ld      e,1ah
1842 7e        ld      a,(hl)
1843 ba        cp      d
1844 2804      jr      z,184ah
1846 2b        dec     hl
1847 1d        dec     e
1848 20f8      jr      nz,1842h
184a 1600      ld      d,00h
184c 3e02      ld      a,02h
184e 211201    ld      hl,0112h
1851 cd9117    call    1791h
1854 300a      jr      nc,1860h
1856 c1        pop     bc
1857 2a78a3    ld      hl,(0a378h)
185a 70        ld      (hl),b
185b af        xor     a
185c 327aa3    ld      (0a37ah),a
185f c9        ret     
1860 cd1b19    call    191bh
1863 c1        pop     bc
1864 2a78a3    ld      hl,(0a378h)
1867 70        ld      (hl),b
1868 3e00      ld      a,00h
186a 327aa3    ld      (0a37ah),a
186d 38bc      jr      c,182bh
186f 7b        ld      a,e
1870 fe1b      cp      1bh
1872 30b7      jr      nc,182bh
1874 1600      ld      d,00h
1876 218818    ld      hl,1888h
1879 19        add     hl,de
187a 7e        ld      a,(hl)
187b f640      or      40h
187d 2a78a3    ld      hl,(0a378h)
1880 77        ld      (hl),a
1881 af        xor     a
1882 327aa3    ld      (0a37ah),a
1885 c30718    jp      1807h
1888 14        inc     d
1889 04        inc     b
188a 15        dec     d
188b 1c        inc     e
188c 181d      jr      18abh
188e 0c        inc     c
188f 05        dec     b
1890 0d        dec     c
1891 1606      ld      d,06h
1893 17        rla     
1894 1e00      ld      e,00h
1896 1f        rra     
1897 0e07      ld      c,07h
1899 0f        rrca    
189a 12        ld      (de),a
189b 02        ld      (bc),a
189c 13        inc     de
189d 1a        ld      a,(de)
189e 19        add     hl,de
189f 1b        dec     de
18a0 0a        ld      a,(bc)
18a1 03        inc     bc
18a2 0b        dec     bc
18a3 ef        rst     28h
18a4 82        add     a,d
18a5 1eed      ld      e,0edh
18a7 5b        ld      e,e
18a8 3aa23e    ld      a,(3ea2h)
18ab 05        dec     b
18ac 211700    ld      hl,0017h
18af cd9117    call    1791h
18b2 d8        ret     c
18b3 cdde18    call    18deh
18b6 38ee      jr      c,18a6h
18b8 7a        ld      a,d
18b9 e6c0      and     0c0h
18bb 67        ld      h,a
18bc 3a3ba2    ld      a,(0a23bh)
18bf e6c0      and     0c0h
18c1 bc        cp      h
18c2 2006      jr      nz,18cah
18c4 ed533aa2  ld      (0a23ah),de
18c8 1812      jr      18dch
18ca d5        push    de
18cb cd541c    call    1c54h
18ce cd4a15    call    154ah
18d1 cd321a    call    1a32h
18d4 d1        pop     de
18d5 ed533aa2  ld      (0a23ah),de
18d9 cd011a    call    1a01h
18dc a7        and     a
18dd c9        ret     
18de 210ba3    ld      hl,0a30bh
18e1 3a09a3    ld      a,(0a309h)
18e4 47        ld      b,a
18e5 110000    ld      de,0000h
18e8 7e        ld      a,(hl)
18e9 fe3a      cp      3ah
18eb 302c      jr      nc,1919h
18ed fe30      cp      30h
18ef 3828      jr      c,1919h
18f1 d630      sub     30h
18f3 e5        push    hl
18f4 cb23      sla     e
18f6 cb12      rl      d
18f8 3810      jr      c,190ah
18fa 62        ld      h,d
18fb 6b        ld      l,e
18fc cb23      sla     e
18fe cb12      rl      d
1900 3808      jr      c,190ah
1902 cb23      sla     e
1904 cb12      rl      d
1906 3802      jr      c,190ah
1908 19        add     hl,de
1909 eb        ex      de,hl
190a e1        pop     hl
190b 380c      jr      c,1919h
190d 83        add     a,e
190e 5f        ld      e,a
190f 3003      jr      nc,1914h
1911 14        inc     d
1912 2805      jr      z,1919h
1914 23        inc     hl
1915 10d1      djnz    18e8h
1917 a7        and     a
1918 c9        ret     
1919 37        scf     
191a c9        ret     
191b cdde18    call    18deh
191e d8        ret     c
191f af        xor     a
1920 ba        cp      d
1921 c8        ret     z
1922 37        scf     
1923 c9        ret     
1924 ef        rst     28h
1925 cc1d3e    call    z,3e1dh
1928 08        ex      af,af'
1929 211000    ld      hl,0010h
192c cdc016    call    16c0h
192f d8        ret     c
1930 212ba3    ld      hl,0a32bh
1933 060e      ld      b,0eh
1935 3620      ld      (hl),20h
1937 23        inc     hl
1938 10fb      djnz    1935h
193a 3e3a      ld      a,3ah
193c 322ca3    ld      (0a32ch),a
193f 210ba3    ld      hl,0a30bh
1942 112da3    ld      de,0a32dh
1945 ed4b09a3  ld      bc,(0a309h)
1949 0600      ld      b,00h
194b edb0      ldir    
194d 213300    ld      hl,0033h
1950 1135a3    ld      de,0a335h
1953 010400    ld      bc,0004h
1956 edb0      ldir    
1958 af        xor     a
1959 326ea3    ld      (0a36eh),a
195c cd4c00    call    004ch
195f 2820      jr      z,1981h
1961 f7        rst     30h
1962 dc1d3e    call    c,3e1dh
1965 012114    ld      bc,1421h
1968 01cdc0    ld      bc,0c0cdh
196b 16d8      ld      d,0d8h
196d 3a0ba3    ld      a,(0a30bh)
1970 322ba3    ld      (0a32bh),a
1973 fe43      cp      43h
1975 3006      jr      nc,197dh
1977 fe41      cp      41h
1979 38e9      jr      c,1964h
197b a7        and     a
197c c9        ret     
197d fe54      cp      54h
197f 20e3      jr      nz,1964h
1981 af        xor     a
1982 326fa3    ld      (0a36fh),a
1985 2f        cpl     
1986 326ea3    ld      (0a36eh),a
1989 cd5b00    call    005bh
198c 2824      jr      z,19b2h
198e ef        rst     28h
198f ff        rst     38h
1990 1d        dec     e
1991 3e01      ld      a,01h
1993 212000    ld      hl,0020h
1996 cdc016    call    16c0h
1999 d8        ret     c
199a 3a0ba3    ld      a,(0a30bh)
199d fe53      cp      53h
199f 2811      jr      z,19b2h
19a1 fe46      cp      46h
19a3 2004      jr      nz,19a9h
19a5 3e01      ld      a,01h
19a7 1806      jr      19afh
19a9 fe54      cp      54h
19ab 20e4      jr      nz,1991h
19ad 3e02      ld      a,02h
19af 326fa3    ld      (0a36fh),a
19b2 cd5b00    call    005bh
19b5 2805      jr      z,19bch
19b7 ef        rst     28h
19b8 e0        ret     po
19b9 1e18      ld      e,18h
19bb 03        inc     bc
19bc ef        rst     28h
19bd c7        rst     00h
19be 1ecd      ld      e,0cdh
19c0 1114a7    ld      de,0a714h
19c3 c9        ret     
19c4 f3        di      
19c5 210040    ld      hl,4000h
19c8 1600      ld      d,00h
19ca cdd119    call    19d1h
19cd fb        ei      
19ce c9        ret     
19cf 1601      ld      d,01h
19d1 06fe      ld      b,0feh
19d3 4a        ld      c,d
19d4 ed49      out     (c),c
19d6 0100fc    ld      bc,0fc00h
19d9 5e        ld      e,(hl)
19da ed78      in      a,(c)
19dc 77        ld      (hl),a
19dd ed59      out     (c),e
19df 23        inc     hl
19e0 03        inc     bc
19e1 78        ld      a,b
19e2 fefe      cp      0feh
19e4 c2d919    jp      nz,19d9h
19e7 14        inc     d
19e8 7a        ld      a,d
19e9 fe10      cp      10h
19eb 20e4      jr      nz,19d1h
19ed c9        ret     
19ee 0100fe    ld      bc,0fe00h
19f1 ed49      out     (c),c
19f3 0100fd    ld      bc,0fd00h
19f6 5e        ld      e,(hl)
19f7 ed78      in      a,(c)
19f9 77        ld      (hl),a
19fa ed59      out     (c),e
19fc 23        inc     hl
19fd 0c        inc     c
19fe 20f6      jr      nz,19f6h
1a00 c9        ret     
1a01 cd531a    call    1a53h
1a04 f3        di      
1a05 067f      ld      b,7fh
1a07 3a9ba3    ld      a,(0a39bh)
1a0a f68c      or      8ch
1a0c ed79      out     (c),a
1a0e 3e08      ld      a,08h
1a10 2100c0    ld      hl,0c000h
1a13 1100a4    ld      de,0a400h
1a16 019001    ld      bc,0190h
1a19 edb0      ldir    
1a1b 6f        ld      l,a
1a1c 3e07      ld      a,07h
1a1e 84        add     a,h
1a1f 67        ld      h,a
1a20 7d        ld      a,l
1a21 2e00      ld      l,00h
1a23 3d        dec     a
1a24 20f0      jr      nz,1a16h
1a26 067f      ld      b,7fh
1a28 3a9ba3    ld      a,(0a39bh)
1a2b ed79      out     (c),a
1a2d fb        ei      
1a2e cd4b1c    call    1c4bh
1a31 c9        ret     
1a32 cd541c    call    1c54h
1a35 f3        di      
1a36 3e08      ld      a,08h
1a38 2100a4    ld      hl,0a400h
1a3b 1100c0    ld      de,0c000h
1a3e 019001    ld      bc,0190h
1a41 edb0      ldir    
1a43 5f        ld      e,a
1a44 3e07      ld      a,07h
1a46 82        add     a,d
1a47 57        ld      d,a
1a48 7b        ld      a,e
1a49 1e00      ld      e,00h
1a4b 3d        dec     a
1a4c 20f0      jr      nz,1a3eh
1a4e fb        ei      
1a4f cd531a    call    1a53h
1a52 c9        ret     
1a53 3a3ba2    ld      a,(0a23bh)
1a56 e6c0      and     0c0h
1a58 fec0      cp      0c0h
1a5a c8        ret     z
1a5b f5        push    af
1a5c cd4a15    call    154ah
1a5f f1        pop     af
1a60 fe80      cp      80h
1a62 280a      jr      z,1a6eh
1a64 67        ld      h,a
1a65 2e00      ld      l,00h
1a67 010040    ld      bc,4000h
1a6a cd4b09    call    094bh
1a6d c9        ret     
1a6e 210080    ld      hl,8000h
1a71 010021    ld      bc,2100h
1a74 cd4b09    call    094bh
1a77 f3        di      
1a78 018d7f    ld      bc,7f8dh
1a7b ed49      out     (c),c
1a7d 2100e1    ld      hl,0e100h
1a80 cdcf19    call    19cfh
1a83 01857f    ld      bc,7f85h
1a86 ed49      out     (c),c
1a88 fb        ei      
1a89 c9        ret     
1a8a 0e08      ld      c,08h
1a8c 2100c0    ld      hl,0c000h
1a8f 119001    ld      de,0190h
1a92 3600      ld      (hl),00h
1a94 23        inc     hl
1a95 1b        dec     de
1a96 7a        ld      a,d
1a97 b3        or      e
1a98 20f8      jr      nz,1a92h
1a9a 3e07      ld      a,07h
1a9c 84        add     a,h
1a9d 67        ld      h,a
1a9e 2e00      ld      l,00h
1aa0 0d        dec     c
1aa1 20ec      jr      nz,1a8fh
1aa3 af        xor     a
1aa4 3205a3    ld      (0a305h),a
1aa7 3204a3    ld      (0a304h),a
1aaa 3203a3    ld      (0a303h),a
1aad c9        ret     
1aae 2100c0    ld      hl,0c000h
1ab1 af        xor     a
1ab2 77        ld      (hl),a
1ab3 23        inc     hl
1ab4 bc        cp      h
1ab5 20fb      jr      nz,1ab2h
1ab7 18ea      jr      1aa3h
1ab9 dde1      pop     ix
1abb dd6e00    ld      l,(ix+00h)
1abe dd6601    ld      h,(ix+01h)
1ac1 dd23      inc     ix
1ac3 dd23      inc     ix
1ac5 dde5      push    ix
1ac7 7e        ld      a,(hl)
1ac8 feff      cp      0ffh
1aca c8        ret     z
1acb cd6e15    call    156eh
1ace 23        inc     hl
1acf 18f6      jr      1ac7h
1ad1 e5        push    hl
1ad2 f3        di      
1ad3 2100a1    ld      hl,0a100h
1ad6 cdcf19    call    19cfh
1ad9 0100fe    ld      bc,0fe00h
1adc ed49      out     (c),c
1ade 0140fc    ld      bc,0fc40h
1ae1 af        xor     a
1ae2 ed79      out     (c),a
1ae4 cd6213    call    1362h
1ae7 0100df    ld      bc,0df00h
1aea ed49      out     (c),c
1aec 01817f    ld      bc,7f81h
1aef ed49      out     (c),c
1af1 c31620    jp      2016h
1af4 2a0600    ld      hl,(0006h)
1af7 013300    ld      bc,0033h
1afa 09        add     hl,bc
1afb 0600      ld      b,00h
1afd 4e        ld      c,(hl)
1afe 23        inc     hl
1aff 09        add     hl,bc
1b00 23        inc     hl
1b01 5e        ld      e,(hl)
1b02 23        inc     hl
1b03 56        ld      d,(hl)
1b04 010500    ld      bc,0005h
1b07 09        add     hl,bc
1b08 0600      ld      b,00h
1b0a 4e        ld      c,(hl)
1b0b 23        inc     hl
1b0c 09        add     hl,bc
1b0d e5        push    hl
1b0e 212080    ld      hl,8020h
1b11 0e00      ld      c,00h
1b13 c9        ret     
1b14 f3        di      
1b15 e5        push    hl
1b16 d5        push    de
1b17 c5        push    bc
1b18 3afe80    ld      a,(80feh)
1b1b a7        and     a
1b1c 281c      jr      z,1b3ah
1b1e 01c47f    ld      bc,7fc4h
1b21 ed49      out     (c),c
1b23 210040    ld      hl,4000h
1b26 3e80      ld      a,80h
1b28 3600      ld      (hl),00h
1b2a 23        inc     hl
1b2b bc        cp      h
1b2c c23480    jp      nz,8034h
1b2f 0c        inc     c
1b30 79        ld      a,c
1b31 fec8      cp      0c8h
1b33 20ec      jr      nz,1b21h
1b35 01c07f    ld      bc,7fc0h
1b38 ed49      out     (c),c
1b3a 3aff80    ld      a,(80ffh)
1b3d a7        and     a
1b3e 2805      jr      z,1b45h
1b40 3ec9      ld      a,0c9h
1b42 32cbbc    ld      (0bccbh),a
1b45 c1        pop     bc
1b46 d1        pop     de
1b47 e1        pop     hl
1b48 fb        ei      
1b49 c306c0    jp      0c006h
1b4c 00        nop     
1b4d 0600      ld      b,00h
1b4f 111027    ld      de,2710h
1b52 cd701b    call    1b70h
1b55 11e803    ld      de,03e8h
1b58 cd701b    call    1b70h
1b5b 116400    ld      de,0064h
1b5e cd701b    call    1b70h
1b61 110a00    ld      de,000ah
1b64 cd701b    call    1b70h
1b67 06ff      ld      b,0ffh
1b69 110100    ld      de,0001h
1b6c cd701b    call    1b70h
1b6f c9        ret     
1b70 0e00      ld      c,00h
1b72 a7        and     a
1b73 ed52      sbc     hl,de
1b75 3803      jr      c,1b7ah
1b77 0c        inc     c
1b78 18f8      jr      1b72h
1b7a 19        add     hl,de
1b7b 79        ld      a,c
1b7c b0        or      b
1b7d c8        ret     z
1b7e 06ff      ld      b,0ffh
1b80 e5        push    hl
1b81 210ba3    ld      hl,0a30bh
1b84 3a0aa3    ld      a,(0a30ah)
1b87 5f        ld      e,a
1b88 1600      ld      d,00h
1b8a 19        add     hl,de
1b8b 3e30      ld      a,30h
1b8d 81        add     a,c
1b8e 77        ld      (hl),a
1b8f 7b        ld      a,e
1b90 3c        inc     a
1b91 320aa3    ld      (0a30ah),a
1b94 e1        pop     hl
1b95 c9        ret     
1b96 f5        push    af
1b97 3afebf    ld      a,(0bffeh)
1b9a a7        and     a
1b9b c2a41b    jp      nz,1ba4h
1b9e f1        pop     af
1b9f fb        ei      
1ba0 c9        ret     
1ba1 e1        pop     hl
1ba2 18fa      jr      1b9eh
1ba4 e5        push    hl
1ba5 d5        push    de
1ba6 c5        push    bc
1ba7 01fff5    ld      bc,0f5ffh
1baa ed78      in      a,(c)
1bac 1f        rra     
1bad d2d31b    jp      nc,1bd3h
1bb0 af        xor     a
1bb1 329ca3    ld      (0a39ch),a
1bb4 3a9aa3    ld      a,(0a39ah)
1bb7 a7        and     a
1bb8 2850      jr      z,1c0ah
1bba 01007f    ld      bc,7f00h
1bbd 3e85      ld      a,85h
1bbf ed79      out     (c),a
1bc1 329da3    ld      (0a39dh),a
1bc4 0101bc    ld      bc,0bc01h
1bc7 ed49      out     (c),c
1bc9 0128bd    ld      bc,0bd28h
1bcc ed49      out     (c),c
1bce cd5815    call    1558h
1bd1 1837      jr      1c0ah
1bd3 3a9ca3    ld      a,(0a39ch)
1bd6 3c        inc     a
1bd7 329ca3    ld      (0a39ch),a
1bda fe02      cp      02h
1bdc 202c      jr      nz,1c0ah
1bde 3a9aa3    ld      a,(0a39ah)
1be1 a7        and     a
1be2 2826      jr      z,1c0ah
1be4 067f      ld      b,7fh
1be6 3a9ba3    ld      a,(0a39bh)
1be9 ed79      out     (c),a
1beb 329da3    ld      (0a39dh),a
1bee 0101bc    ld      bc,0bc01h
1bf1 ed49      out     (c),c
1bf3 06bd      ld      b,0bdh
1bf5 3a27a2    ld      a,(0a227h)
1bf8 ed79      out     (c),a
1bfa 3a29a2    ld      a,(0a229h)
1bfd 57        ld      d,a
1bfe 3e44      ld      a,44h
1c00 3229a2    ld      (0a229h),a
1c03 cd5d15    call    155dh
1c06 7a        ld      a,d
1c07 3229a2    ld      (0a229h),a
1c0a c1        pop     bc
1c0b d1        pop     de
1c0c e1        pop     hl
1c0d 3a3ea3    ld      a,(0a33eh)
1c10 a7        and     a
1c11 288b      jr      z,1b9eh
1c13 3a9ca3    ld      a,(0a39ch)
1c16 fe02      cp      02h
1c18 2084      jr      nz,1b9eh
1c1a e5        push    hl
1c1b 2a3ca3    ld      hl,(0a33ch)
1c1e 23        inc     hl
1c1f 23        inc     hl
1c20 7e        ld      a,(hl)
1c21 3d        dec     a
1c22 77        ld      (hl),a
1c23 c2a11b    jp      nz,1ba1h
1c26 23        inc     hl
1c27 7e        ld      a,(hl)
1c28 a7        and     a
1c29 2805      jr      z,1c30h
1c2b 3d        dec     a
1c2c 77        ld      (hl),a
1c2d c3a11b    jp      1ba1h
1c30 af        xor     a
1c31 323ea3    ld      (0a33eh),a
1c34 d5        push    de
1c35 c5        push    bc
1c36 21461c    ld      hl,1c46h
1c39 e5        push    hl
1c3a 2a3ca3    ld      hl,(0a33ch)
1c3d 110a00    ld      de,000ah
1c40 19        add     hl,de
1c41 5e        ld      e,(hl)
1c42 23        inc     hl
1c43 56        ld      d,(hl)
1c44 d5        push    de
1c45 c9        ret     
1c46 c1        pop     bc
1c47 d1        pop     de
1c48 c3a11b    jp      1ba1h
1c4b 3eff      ld      a,0ffh
1c4d 329aa3    ld      (0a39ah),a
1c50 32febf    ld      (0bffeh),a
1c53 c9        ret     
1c54 76        halt    
1c55 3a9ca3    ld      a,(0a39ch)
1c58 a7        and     a
1c59 20f9      jr      nz,1c54h
1c5b af        xor     a
1c5c 329aa3    ld      (0a39ah),a
1c5f c9        ret     
1c60 3e07      ld      a,07h
1c62 c9        ret     
1c63 3600      ld      (hl),00h
1c65 23        inc     hl
1c66 3600      ld      (hl),00h
1c68 23        inc     hl
1c69 3600      ld      (hl),00h
1c6b 23        inc     hl
1c6c 70        ld      (hl),b
1c6d 23        inc     hl
1c6e 73        ld      (hl),e
1c6f 23        inc     hl
1c70 72        ld      (hl),d
1c71 23        inc     hl
1c72 71        ld      (hl),c
1c73 23        inc     hl
1c74 c9        ret     
1c75 af        xor     a
1c76 323ea3    ld      (0a33eh),a
1c79 c9        ret     
1c7a 223ca3    ld      (0a33ch),hl
1c7d 3600      ld      (hl),00h
1c7f 23        inc     hl
1c80 3600      ld      (hl),00h
1c82 23        inc     hl
1c83 73        ld      (hl),e
1c84 23        inc     hl
1c85 72        ld      (hl),d
1c86 23        inc     hl
1c87 71        ld      (hl),c
1c88 23        inc     hl
1c89 70        ld      (hl),b
1c8a 23        inc     hl
1c8b 3eff      ld      a,0ffh
1c8d 323ea3    ld      (0a33eh),a
1c90 32febf    ld      (0bffeh),a
1c93 fb        ei      
1c94 c9        ret     
1c95 edb0      ldir    
1c97 c9        ret     
1c98 edb8      lddr    
1c9a c9        ret     
1c9b a7        and     a
1c9c 210000    ld      hl,0000h
1c9f 1627      ld      d,27h
1ca1 1e17      ld      e,17h
1ca3 c9        ret     
1ca4 3d        dec     a
1ca5 3203a3    ld      (0a303h),a
1ca8 c9        ret     
1ca9 c9        ret     
1caa 37        scf     
1cab c9        ret     
1cac 210101    ld      hl,0101h
1caf af        xor     a
1cb0 c9        ret     
1cb1 f5        push    af
1cb2 3a68a3    ld      a,(0a368h)
1cb5 a7        and     a
1cb6 2002      jr      nz,1cbah
1cb8 f1        pop     af
1cb9 c9        ret     
1cba f1        pop     af
1cbb c36e15    jp      156eh
1cbe f3        di      
1cbf 08        ex      af,af'
1cc0 d9        exx     
1cc1 e1        pop     hl
1cc2 5e        ld      e,(hl)
1cc3 23        inc     hl
1cc4 56        ld      d,(hl)
1cc5 23        inc     hl
1cc6 e5        push    hl
1cc7 0107df    ld      bc,0df07h
1cca c5        push    bc
1ccb fde5      push    iy
1ccd 21de1c    ld      hl,1cdeh
1cd0 e5        push    hl
1cd1 eb        ex      de,hl
1cd2 5e        ld      e,(hl)
1cd3 23        inc     hl
1cd4 56        ld      d,(hl)
1cd5 d5        push    de
1cd6 fd2a01a3  ld      iy,(0a301h)
1cda 08        ex      af,af'
1cdb d9        exx     
1cdc fb        ei      
1cdd c9        ret     
1cde f3        di      
1cdf d9        exx     
1ce0 fde1      pop     iy
1ce2 c1        pop     bc
1ce3 d9        exx     
1ce4 fb        ei      
1ce5 c9        ret     
1ce6 c9        ret     
1ce7 01201d    ld      bc,1d20h
1cea 204d      jr      nz,1d39h
1cec 69        ld      l,c
1ced 72        ld      (hl),d
1cee 61        ld      h,c
1cef 67        ld      h,a
1cf0 65        ld      h,l
1cf1 2000      jr      nz,1cf3h
1cf3 2056      jr      nz,1d4bh
1cf5 65        ld      h,l
1cf6 72        ld      (hl),d
1cf7 73        ld      (hl),e
1cf8 69        ld      l,c
1cf9 6f        ld      l,a
1cfa 6e        ld      l,(hl)
1cfb 2033      jr      nz,1d30h
1cfd 2e32      ld      l,32h
1cff 2020      jr      nz,1d21h
1d01 2020      jr      nz,1d23h
1d03 2020      jr      nz,1d25h
1d05 2020      jr      nz,1d27h
1d07 01201d    ld      bc,1d20h
1d0a 204d      jr      nz,1d59h
1d0c 69        ld      l,c
1d0d 72        ld      (hl),d
1d0e 61        ld      h,c
1d0f 67        ld      h,a
1d10 65        ld      h,l
1d11 2000      jr      nz,1d13h
1d13 d31e      out     (1eh),a
1d15 2049      jr      nz,1d60h
1d17 6d        ld      l,l
1d18 61        ld      h,c
1d19 67        ld      h,a
1d1a 65        ld      h,l
1d1b 72        ld      (hl),d
1d1c d1        pop     de
1d1d 20a4      jr      nz,1cc3h
1d1f 313938    ld      sp,3839h
1d22 37        scf     
1d23 204d      jr      nz,1d72h
1d25 2e42      ld      l,42h
1d27 2e44      ld      l,44h
1d29 61        ld      h,c
1d2a 6e        ld      l,(hl)
1d2b 69        ld      l,c
1d2c 65        ld      h,l
1d2d 6c        ld      l,h
1d2e 6c        ld      l,h
1d2f 73        ld      (hl),e
1d30 20d3      jr      nz,1d05h
1d32 1e20      ld      e,20h
1d34 49        ld      c,c
1d35 6d        ld      l,l
1d36 61        ld      h,c
1d37 67        ld      h,a
1d38 65        ld      h,l
1d39 72        ld      (hl),d
1d3a d1        pop     de
1d3b 1f        rra     
1d3c 1f        rra     
1d3d 1f        rra     
1d3e 1f        rra     
1d3f 1f        rra     
1d40 1f        rra     
1d41 1f        rra     
1d42 1f        rra     
1d43 1f        rra     
1d44 1f        rra     
1d45 2020      jr      nz,1d67h
1d47 2020      jr      nz,1d69h
1d49 2020      jr      nz,1d6bh
1d4b 2020      jr      nz,1d6dh
1d4d 2020      jr      nz,1d6fh
1d4f 2020      jr      nz,1d71h
1d51 2020      jr      nz,1d73h
1d53 2020      jr      nz,1d75h
1d55 2020      jr      nz,1d77h
1d57 2020      jr      nz,1d79h
1d59 1f        rra     
1d5a 1f        rra     
1d5b 1f        rra     
1d5c 1f        rra     
1d5d 1f        rra     
1d5e 1f        rra     
1d5f 1f        rra     
1d60 1f        rra     
1d61 1f        rra     
1d62 1f        rra     
1d63 ff        rst     38h
1d64 4c        ld      c,h
1d65 6f        ld      l,a
1d66 61        ld      h,c
1d67 64        ld      h,h
1d68 2053      jr      nz,1dbdh
1d6a 61        ld      h,c
1d6b 76        halt    
1d6c 65        ld      h,l
1d6d 2052      jr      nz,1dc1h
1d6f 75        ld      (hl),l
1d70 6e        ld      l,(hl)
1d71 204e      jr      nz,1dc1h
1d73 65        ld      h,l
1d74 77        ld      (hl),a
1d75 2057      jr      nz,1dceh
1d77 69        ld      l,c
1d78 6e        ld      l,(hl)
1d79 64        ld      h,h
1d7a 6f        ld      l,a
1d7b 77        ld      (hl),a
1d7c 2041      jr      nz,1dbfh
1d7e 64        ld      h,h
1d7f 64        ld      h,h
1d80 72        ld      (hl),d
1d81 65        ld      h,l
1d82 73        ld      (hl),e
1d83 73        ld      (hl),e
1d84 2049      jr      nz,1dcfh
1d86 6e        ld      l,(hl)
1d87 6b        ld      l,e
1d88 20ff      jr      nz,1d89h
1d8a 43        ld      b,e
1d8b 61        ld      h,c
1d8c 74        ld      (hl),h
1d8d ff        rst     38h
1d8e 53        ld      d,e
1d8f 65        ld      h,l
1d90 6c        ld      l,h
1d91 65        ld      h,l
1d92 63        ld      h,e
1d93 74        ld      (hl),h
1d94 2072      jr      nz,1e08h
1d96 65        ld      h,l
1d97 71        ld      (hl),c
1d98 75        ld      (hl),l
1d99 69        ld      l,c
1d9a 72        ld      (hl),d
1d9b 65        ld      h,l
1d9c 64        ld      h,h
1d9d 2066      jr      nz,1e05h
1d9f 75        ld      (hl),l
1da0 6e        ld      l,(hl)
1da1 63        ld      h,e
1da2 74        ld      (hl),h
1da3 69        ld      l,c
1da4 6f        ld      l,a
1da5 6e        ld      l,(hl)
1da6 3aff4d    ld      a,(4dffh)
1da9 69        ld      l,c
1daa 72        ld      (hl),d
1dab 61        ld      h,c
1dac 67        ld      h,a
1dad 65        ld      h,l
1dae 2049      jr      nz,1df9h
1db0 6d        ld      l,l
1db1 61        ld      h,c
1db2 67        ld      h,a
1db3 65        ld      h,l
1db4 72        ld      (hl),d
1db5 2028      jr      nz,1ddfh
1db7 63        ld      h,e
1db8 29        add     hl,hl
1db9 2031      jr      nz,1dech
1dbb 39        add     hl,sp
1dbc 3836      jr      c,1df4h
1dbe 204d      jr      nz,1e0dh
1dc0 2e42      ld      l,42h
1dc2 2e44      ld      l,44h
1dc4 61        ld      h,c
1dc5 6e        ld      l,(hl)
1dc6 69        ld      l,c
1dc7 65        ld      h,l
1dc8 6c        ld      l,h
1dc9 6c        ld      l,h
1dca 73        ld      (hl),e
1dcb 2e45      ld      l,45h
1dcd 6e        ld      l,(hl)
1dce 74        ld      (hl),h
1dcf 65        ld      h,l
1dd0 72        ld      (hl),d
1dd1 2066      jr      nz,1e39h
1dd3 69        ld      l,c
1dd4 6c        ld      l,h
1dd5 65        ld      h,l
1dd6 6e        ld      l,(hl)
1dd7 61        ld      h,c
1dd8 6d        ld      l,l
1dd9 65        ld      h,l
1dda 3aff0d    ld      a,(0dffh)
1ddd 44        ld      b,h
1dde 72        ld      (hl),d
1ddf 69        ld      l,c
1de0 76        halt    
1de1 65        ld      h,l
1de2 2041      jr      nz,1e25h
1de4 2c        inc     l
1de5 2042      jr      nz,1e29h
1de7 206f      jr      nz,1e58h
1de9 72        ld      (hl),d
1dea 2054      jr      nz,1e40h
1dec 61        ld      h,c
1ded 70        ld      (hl),b
1dee 65        ld      h,l
1def 3f        ccf     
1df0 ff        rst     38h
1df1 44        ld      b,h
1df2 72        ld      (hl),d
1df3 69        ld      l,c
1df4 76        halt    
1df5 65        ld      h,l
1df6 2041      jr      nz,1e39h
1df8 206f      jr      nz,1e69h
1dfa 72        ld      (hl),d
1dfb 2042      jr      nz,1e3fh
1dfd 3f        ccf     
1dfe ff        rst     38h
1dff 53        ld      d,e
1e00 6c        ld      l,h
1e01 6f        ld      l,a
1e02 77        ld      (hl),a
1e03 2c        inc     l
1e04 2046      jr      nz,1e4ch
1e06 61        ld      h,c
1e07 73        ld      (hl),e
1e08 74        ld      (hl),h
1e09 206f      jr      nz,1e7ah
1e0b 72        ld      (hl),d
1e0c 2054      jr      nz,1e62h
1e0e 75        ld      (hl),l
1e0f 72        ld      (hl),d
1e10 62        ld      h,d
1e11 6f        ld      l,a
1e12 2074      jr      nz,1e88h
1e14 61        ld      h,c
1e15 70        ld      (hl),b
1e16 65        ld      h,l
1e17 2073      jr      nz,1e8ch
1e19 70        ld      (hl),b
1e1a 65        ld      h,l
1e1b 65        ld      h,l
1e1c 64        ld      h,h
1e1d 3f        ccf     
1e1e ff        rst     38h
1e1f 44        ld      b,h
1e20 69        ld      l,c
1e21 73        ld      (hl),e
1e22 61        ld      h,c
1e23 62        ld      h,d
1e24 6c        ld      l,h
1e25 65        ld      h,l
1e26 2065      jr      nz,1e8dh
1e28 78        ld      a,b
1e29 70        ld      (hl),b
1e2a 61        ld      h,c
1e2b 6e        ld      l,(hl)
1e2c 73        ld      (hl),e
1e2d 69        ld      l,c
1e2e 6f        ld      l,a
1e2f 6e        ld      l,(hl)
1e30 2052      jr      nz,1e84h
1e32 4f        ld      c,a
1e33 4d        ld      c,l
1e34 73        ld      (hl),e
1e35 3f        ccf     
1e36 2028      jr      nz,1e60h
1e38 59        ld      e,c
1e39 2f        cpl     
1e3a 4e        ld      c,(hl)
1e3b 29        add     hl,hl
1e3c ff        rst     38h
1e3d 45        ld      b,l
1e3e 6e        ld      l,(hl)
1e3f 74        ld      (hl),h
1e40 65        ld      h,l
1e41 72        ld      (hl),d
1e42 2064      jr      nz,1ea8h
1e44 69        ld      l,c
1e45 73        ld      (hl),e
1e46 70        ld      (hl),b
1e47 6c        ld      l,h
1e48 61        ld      h,c
1e49 79        ld      a,c
1e4a 2077      jr      nz,1ec3h
1e4c 69        ld      l,c
1e4d 64        ld      h,h
1e4e 74        ld      (hl),h
1e4f 68        ld      l,b
1e50 3aff0d    ld      a,(0dffh)
1e53 45        ld      b,l
1e54 6e        ld      l,(hl)
1e55 74        ld      (hl),h
1e56 65        ld      h,l
1e57 72        ld      (hl),d
1e58 2064      jr      nz,1ebeh
1e5a 69        ld      l,c
1e5b 73        ld      (hl),e
1e5c 70        ld      (hl),b
1e5d 6c        ld      l,h
1e5e 61        ld      h,c
1e5f 79        ld      a,c
1e60 2068      jr      nz,1ecah
1e62 65        ld      h,l
1e63 69        ld      l,c
1e64 67        ld      h,a
1e65 68        ld      l,b
1e66 74        ld      (hl),h
1e67 3aff45    ld      a,(45ffh)
1e6a 6e        ld      l,(hl)
1e6b 74        ld      (hl),h
1e6c 65        ld      h,l
1e6d 72        ld      (hl),d
1e6e 2076      jr      nz,1ee6h
1e70 65        ld      h,l
1e71 72        ld      (hl),d
1e72 74        ld      (hl),h
1e73 69        ld      l,c
1e74 63        ld      h,e
1e75 61        ld      h,c
1e76 6c        ld      l,h
1e77 2070      jr      nz,1ee9h
1e79 6f        ld      l,a
1e7a 73        ld      (hl),e
1e7b 69        ld      l,c
1e7c 74        ld      (hl),h
1e7d 69        ld      l,c
1e7e 6f        ld      l,a
1e7f 6e        ld      l,(hl)
1e80 3aff45    ld      a,(45ffh)
1e83 6e        ld      l,(hl)
1e84 74        ld      (hl),h
1e85 65        ld      h,l
1e86 72        ld      (hl),d
1e87 2064      jr      nz,1eedh
1e89 69        ld      l,c
1e8a 73        ld      (hl),e
1e8b 70        ld      (hl),b
1e8c 6c        ld      l,h
1e8d 61        ld      h,c
1e8e 79        ld      a,c
1e8f 2061      jr      nz,1ef2h
1e91 64        ld      h,h
1e92 64        ld      h,h
1e93 72        ld      (hl),d
1e94 65        ld      h,l
1e95 73        ld      (hl),e
1e96 73        ld      (hl),e
1e97 3aff45    ld      a,(45ffh)
1e9a 6e        ld      l,(hl)
1e9b 74        ld      (hl),h
1e9c 65        ld      h,l
1e9d 72        ld      (hl),d
1e9e 2070      jr      nz,1f10h
1ea0 65        ld      h,l
1ea1 6e        ld      l,(hl)
1ea2 206e      jr      nz,1f12h
1ea4 75        ld      (hl),l
1ea5 6d        ld      l,l
1ea6 62        ld      h,d
1ea7 65        ld      h,l
1ea8 72        ld      (hl),d
1ea9 206f      jr      nz,1f1ah
1eab 72        ld      (hl),d
1eac 205b      jr      nz,1f09h
1eae 45        ld      b,l
1eaf 53        ld      d,e
1eb0 43        ld      b,e
1eb1 5d        ld      e,l
1eb2 3aff0d    ld      a,(0dffh)
1eb5 45        ld      b,l
1eb6 6e        ld      l,(hl)
1eb7 74        ld      (hl),h
1eb8 65        ld      h,l
1eb9 72        ld      (hl),d
1eba 2069      jr      nz,1f25h
1ebc 6e        ld      l,(hl)
1ebd 6b        ld      l,e
1ebe 206e      jr      nz,1f2eh
1ec0 75        ld      (hl),l
1ec1 6d        ld      l,l
1ec2 62        ld      h,d
1ec3 65        ld      h,l
1ec4 72        ld      (hl),d
1ec5 3aff50    ld      a,(50ffh)
1ec8 72        ld      (hl),d
1ec9 65        ld      h,l
1eca 73        ld      (hl),e
1ecb 73        ld      (hl),e
1ecc 2050      jr      nz,1f1eh
1ece 4c        ld      c,h
1ecf 41        ld      b,c
1ed0 59        ld      e,c
1ed1 2074      jr      nz,1f47h
1ed3 68        ld      l,b
1ed4 65        ld      h,l
1ed5 6e        ld      l,(hl)
1ed6 205b      jr      nz,1f33h
1ed8 45        ld      b,l
1ed9 4e        ld      c,(hl)
1eda 54        ld      d,h
1edb 45        ld      b,l
1edc 52        ld      d,d
1edd 5d        ld      e,l
1ede 2eff      ld      l,0ffh
1ee0 50        ld      d,b
1ee1 72        ld      (hl),d
1ee2 65        ld      h,l
1ee3 73        ld      (hl),e
1ee4 73        ld      (hl),e
1ee5 2052      jr      nz,1f39h
1ee7 45        ld      b,l
1ee8 43        ld      b,e
1ee9 2061      jr      nz,1f4ch
1eeb 6e        ld      l,(hl)
1eec 64        ld      h,h
1eed 2050      jr      nz,1f3fh
1eef 4c        ld      c,h
1ef0 41        ld      b,c
1ef1 59        ld      e,c
1ef2 2074      jr      nz,1f68h
1ef4 68        ld      l,b
1ef5 65        ld      h,l
1ef6 6e        ld      l,(hl)
1ef7 205b      jr      nz,1f54h
1ef9 45        ld      b,l
1efa 4e        ld      c,(hl)
1efb 54        ld      d,h
1efc 45        ld      b,l
1efd 52        ld      d,d
1efe 5d        ld      e,l
1eff 2eff      ld      l,0ffh
1f01 44        ld      b,h
1f02 69        ld      l,c
1f03 73        ld      (hl),e
1f04 63        ld      h,e
1f05 2065      jr      nz,1f6ch
1f07 72        ld      (hl),d
1f08 72        ld      (hl),d
1f09 6f        ld      l,a
1f0a 72        ld      (hl),d
1f0b 2e20      ld      l,20h
1f0d ff        rst     38h
1f0e 0d        dec     c
1f0f 50        ld      d,b
1f10 72        ld      (hl),d
1f11 65        ld      h,l
1f12 73        ld      (hl),e
1f13 73        ld      (hl),e
1f14 205b      jr      nz,1f71h
1f16 45        ld      b,l
1f17 4e        ld      c,(hl)
1f18 54        ld      d,h
1f19 45        ld      b,l
1f1a 52        ld      d,d
1f1b 5d        ld      e,l
1f1c 2074      jr      nz,1f92h
1f1e 6f        ld      l,a
1f1f 2063      jr      nz,1f84h
1f21 6f        ld      l,a
1f22 6e        ld      l,(hl)
1f23 74        ld      (hl),h
1f24 69        ld      l,c
1f25 6e        ld      l,(hl)
1f26 75        ld      (hl),l
1f27 65        ld      h,l
1f28 2eff      ld      l,0ffh
1f2a 46        ld      b,(hl)
1f2b 69        ld      l,c
1f2c 6c        ld      l,h
1f2d 65        ld      h,l
1f2e 206e      jr      nz,1f9eh
1f30 6f        ld      l,a
1f31 74        ld      (hl),h
1f32 2066      jr      nz,1f9ah
1f34 6f        ld      l,a
1f35 75        ld      (hl),l
1f36 6e        ld      l,(hl)
1f37 64        ld      h,h
1f38 2eff      ld      l,0ffh
1f3a 44        ld      b,h
1f3b 69        ld      l,c
1f3c 73        ld      (hl),e
1f3d 63        ld      h,e
1f3e 2069      jr      nz,1fa9h
1f40 73        ld      (hl),e
1f41 2077      jr      nz,1fbah
1f43 72        ld      (hl),d
1f44 69        ld      l,c
1f45 74        ld      (hl),h
1f46 65        ld      h,l
1f47 2070      jr      nz,1fb9h
1f49 72        ld      (hl),d
1f4a 6f        ld      l,a
1f4b 74        ld      (hl),h
1f4c 65        ld      h,l
1f4d 63        ld      h,e
1f4e 74        ld      (hl),h
1f4f 65        ld      h,l
1f50 64        ld      h,h
1f51 2eff      ld      l,0ffh
1f53 44        ld      b,h
1f54 69        ld      l,c
1f55 73        ld      (hl),e
1f56 63        ld      h,e
1f57 2066      jr      nz,1fbfh
1f59 75        ld      (hl),l
1f5a 6c        ld      l,h
1f5b 6c        ld      l,h
1f5c 2eff      ld      l,0ffh
1f5e 54        ld      d,h
1f5f 61        ld      h,c
1f60 70        ld      (hl),b
1f61 65        ld      h,l
1f62 206c      jr      nz,1fd0h
1f64 6f        ld      l,a
1f65 61        ld      h,c
1f66 64        ld      h,h
1f67 69        ld      l,c
1f68 6e        ld      l,(hl)
1f69 67        ld      h,a
1f6a 2065      jr      nz,1fd1h
1f6c 72        ld      (hl),d
1f6d 72        ld      (hl),d
1f6e 6f        ld      l,a
1f6f 72        ld      (hl),d
1f70 2eff      ld      l,0ffh
1f72 57        ld      d,a
1f73 72        ld      (hl),d
1f74 6f        ld      l,a
1f75 6e        ld      l,(hl)
1f76 67        ld      h,a
1f77 2066      jr      nz,1fdfh
1f79 69        ld      l,c
1f7a 6c        ld      l,h
1f7b 65        ld      h,l
1f7c 2074      jr      nz,1ff2h
1f7e 79        ld      a,c
1f7f 70        ld      (hl),b
1f80 65        ld      h,l
1f81 2eff      ld      l,0ffh
1f83 44        ld      b,h
1f84 69        ld      l,c
1f85 73        ld      (hl),e
1f86 63        ld      h,e
1f87 206e      jr      nz,1ff7h
1f89 6f        ld      l,a
1f8a 74        ld      (hl),h
1f8b 2070      jr      nz,1ffdh
1f8d 72        ld      (hl),d
1f8e 65        ld      h,l
1f8f 73        ld      (hl),e
1f90 65        ld      h,l
1f91 6e        ld      l,(hl)
1f92 74        ld      (hl),h
1f93 2eff      ld      l,0ffh
1f95 42        ld      b,d
1f96 61        ld      h,c
1f97 64        ld      h,h
1f98 2070      jr      nz,200ah
1f9a 72        ld      (hl),d
1f9b 6f        ld      l,a
1f9c 67        ld      h,a
1f9d 72        ld      (hl),d
1f9e 61        ld      h,c
1f9f 6d        ld      l,l
1fa0 2eff      ld      l,0ffh
1fa2 54        ld      d,h
1fa3 61        ld      h,c
1fa4 70        ld      (hl),b
1fa5 65        ld      h,l
1fa6 2073      jr      nz,201bh
1fa8 61        ld      h,c
1fa9 76        halt    
1faa 69        ld      l,c
1fab 6e        ld      l,(hl)
1fac 67        ld      h,a
1fad 2065      jr      nz,2014h
1faf 72        ld      (hl),d
1fb0 72        ld      (hl),d
1fb1 6f        ld      l,a
1fb2 72        ld      (hl),d
1fb3 2eff      ld      l,0ffh
1fb5 4e        ld      c,(hl)
1fb6 6f        ld      l,a
1fb7 74        ld      (hl),h
1fb8 2065      jr      nz,201fh
1fba 6e        ld      l,(hl)
1fbb 6f        ld      l,a
1fbc 75        ld      (hl),l
1fbd 67        ld      h,a
1fbe 68        ld      l,b
1fbf 206d      jr      nz,202eh
1fc1 65        ld      h,l
1fc2 6d        ld      l,l
1fc3 6f        ld      l,a
1fc4 72        ld      (hl),d
1fc5 79        ld      a,c
1fc6 2eff      ld      l,0ffh
1fc8 45        ld      b,l
1fc9 78        ld      a,b
1fca 70        ld      (hl),b
1fcb 61        ld      h,c
1fcc 6e        ld      l,(hl)
1fcd 73        ld      (hl),e
1fce 69        ld      l,c
1fcf 6f        ld      l,a
1fd0 6e        ld      l,(hl)
1fd1 2052      jr      nz,2025h
1fd3 4f        ld      c,a
1fd4 4d        ld      c,l
1fd5 206e      jr      nz,2045h
1fd7 6f        ld      l,a
1fd8 74        ld      (hl),h
1fd9 2070      jr      nz,204bh
1fdb 72        ld      (hl),d
1fdc 65        ld      h,l
1fdd 73        ld      (hl),e
1fde 65        ld      h,l
1fdf 6e        ld      l,(hl)
1fe0 74        ld      (hl),h
1fe1 2eff      ld      l,0ffh
1fe3 00        nop     
1fe4 66        ld      h,(hl)
1fe5 99        sbc     a,c
1fe6 bd        cp      l
1fe7 5a        ld      e,d
1fe8 5a        ld      e,d
1fe9 1818      jr      2003h
1feb 1818      jr      2005h
1fed 5a        ld      e,d
1fee 5a        ld      e,d
1fef bd        cp      l
1ff0 99        sbc     a,c
1ff1 66        ld      h,(hl)
1ff2 00        nop     
1ff3 ff        rst     38h
1ff4 00        nop     
1ff5 00        nop     
1ff6 00        nop     
1ff7 00        nop     
1ff8 00        nop     
1ff9 00        nop     
1ffa 00        nop     
1ffb c9        ret     
1ffc 00        nop     
1ffd 00        nop     
1ffe 00        nop     
1fff 00        nop     

Disassembled 4724 instructions.
